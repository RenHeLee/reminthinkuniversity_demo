package com.project.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;


public abstract class StudentAbstract extends User implements StudentInterface{

	protected int id = getUserId();
	protected String po_co;
	protected String name;
	protected String course_name;
	protected int course_id;
	protected String intake_sem_co;
	protected int intake_sem_id;
	protected String graduate_sem_co;
	protected int graduate_sem_id;
	protected String phone;
	protected String ic;
	protected String address;
	protected String mod_by;
	protected Date mod_date;

	protected ArrayList<Integer> subStuId = new ArrayList<Integer>();
	protected ArrayList<Integer> subId = new ArrayList<Integer>();
	protected ArrayList<String> subName = new ArrayList<String>();
	protected ArrayList<Integer> subCo = new ArrayList<Integer>();
	protected ArrayList<Integer> sec = new ArrayList<Integer>();
	protected ArrayList<String> semCo = new ArrayList<String>();
	protected ArrayList<Integer> semId = new ArrayList<Integer>();
	protected ArrayList<String> modBy = new ArrayList<String>();
	protected ArrayList<Date> modDate  = new ArrayList<Date>();
	protected ArrayList<String> meName = new ArrayList<String>();
	protected ArrayList<Float> marks = new ArrayList<Float>();
	protected ArrayList<Float> total = new ArrayList<Float>();
	protected float totalMark;
	
	public int getId() {
		return id;
	}

	public String getPo_co() {
		return po_co;
	}

	public String getName() {
		return name;
	}

	public String getCourse_name() {
		return course_name;
	}

	public int getCourse_id() {
		return course_id;
	}

	public String getIntake_sem_co() {
		return intake_sem_co;
	}

	public int getIntake_sem_id() {
		return intake_sem_id;
	}

	public String getGraduate_sem_co() {
		return graduate_sem_co;
	}

	public int getGraduate_sem_id() {
		return graduate_sem_id;
	}

	public String getPhone() {
		return phone;
	}

	public String getIc() {
		return ic;
	}

	public String getAddress() {
		return address;
	}

	public String getMod_by() {
		return mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public ArrayList<Integer> getSubStuId() {
		return subStuId;
	}

	public ArrayList<Integer> getSubId() {
		return subId;
	}

	public ArrayList<String> getSubName() {
		return subName;
	}

	public ArrayList<Integer> getSubCo() {
		return subCo;
	}

	public ArrayList<Integer> getSec() {
		return sec;
	}

	public ArrayList<String> getSemCo() {
		return semCo;
	}

	public ArrayList<Integer> getSemId() {
		return semId;
	}

	public ArrayList<String> getModBy() {
		return modBy;
	}

	public ArrayList<Date> getModDate() {
		return modDate;
	}

	public ArrayList<String> getMeName() {
		return meName;
	}

	public ArrayList<Float> getMarks() {
		return marks;
	}

	public ArrayList<Float> getTotal() {
		return total;
	}

	public float getTotalMark() {
		return totalMark;
	}

	@Override
	public void selectStudentInfo(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		mod_by = "a";
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT mod_by FROM student WHERE stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
			mod_by = rs.getNString(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		
		if (mod_by == null) {
			try {
				//prepare sql statement with dynamic value
				String sql = "SELECT po.po_co, stu.stu_name, intake.sem_co, stu.in_sem_id, gra.sem_co, stu.gra_sem_id, stu.pho_num, stu.ic_num, stu.address, stu_id, stu.mod_date FROM student stu INNER JOIN position po ON stu.stu_po = po.po_id  INNER JOIN sem_date intake ON stu.in_sem_id = intake.sem_id INNER JOIN sem_date gra ON stu.gra_sem_id = gra.sem_id WHERE stu.stu_id = ?";
				//Initialize value for column
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, stu_id);
				//get query execute status
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
				po_co = rs.getNString(1);
				name = rs.getNString(2);
				intake_sem_co = rs.getNString(3);
				intake_sem_id = rs.getInt(4);
				graduate_sem_co = rs.getNString(5);
				graduate_sem_id = rs.getInt(6);
				phone = rs.getNString(7);
				ic = rs.getNString(8);
				address = rs.getNString(9);
				mod_by = name + "(" + po_co + rs.getInt(10) + ")";
				mod_date = rs.getTimestamp(11);
				}
			} catch (SQLException e) {
				System.out.println("Fail retrive data");
				e.printStackTrace();
			}
		}else {
			try {
				//prepare sql statement with dynamic value
				String sql = "SELECT po.po_co, stu.stu_name, intake.sem_co, stu.in_sem_id, gra.sem_co, stu.gra_sem_id, stu.pho_num, stu.ic_num, stu.address, sta.sta_name||'('||mod.po_co||stu.mod_by||')', stu.mod_date FROM student stu INNER JOIN staff sta ON sta.sta_id = stu.mod_by INNER JOIN position mod ON sta.sta_po = mod.po_id INNER JOIN position po ON stu.stu_po = po.po_id  INNER JOIN sem_date intake ON stu.in_sem_id = intake.sem_id INNER JOIN sem_date gra ON stu.gra_sem_id = gra.sem_id WHERE stu.stu_id = ?";
				//Initialize value for column
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, stu_id);
				//get query execute status
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
				po_co = rs.getNString(1);
				name = rs.getNString(2);
				intake_sem_co = rs.getNString(3);
				intake_sem_id = rs.getInt(4);
				graduate_sem_co = rs.getNString(5);
				graduate_sem_id = rs.getInt(6);
				phone = rs.getNString(7);
				ic = rs.getNString(8);
				address = rs.getNString(9);
				mod_by = rs.getNString(10);
				mod_date = rs.getTimestamp(11);
				}
			} catch (SQLException e) {
				System.out.println("Fail retrive data");
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean updateStudentInfo(Connection connection, int stu_id, String pho_num, String address) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE student SET pho_num = ?, address = ?, mod_by = ?, mod_date = ? WHERE stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setNString(1, pho_num);
			ps.setNString(2, address);
			ps.setNull(3, java.sql.Types.INTEGER);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(5, stu_id);
			//get query execute status
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void selectAllSubjectBySection(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_li.sub_id, sub.sub_name, sub_li.sub_co, sub_li.sec, sem.sem_co, sub_li.sem_id, sta.sta_name||'('||po.po_co||sub_li.mod_by||')', sub_li.mod_date FROM subject_list sub_li INNER JOIN staff sta ON sub_li.mod_by = sta.sta_id INNER JOIN position po ON sta.sta_po = po.po_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN sem_date sem ON sem.sem_id = sub_li.sem_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subId.clear();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				subId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				subCo.add(rs.getInt(3));
				sec.add(rs.getInt(4));
				semCo.add(rs.getNString(5));
				semId.add(rs.getInt(6));
				modBy.add(rs.getNString(7));
				modDate.add(rs.getTimestamp(8));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean checkStudentSubject(Connection connection, int sub_id, int stu_id) {
		// TODO Auto-generated method stub
		Connection connection2 = databaseConnection();
		System.out.println(connection);
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu_id FROM subject_student WHERE sub_id = ? AND stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection2.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, stu_id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertStudentSubject(Connection connection, int sub_id, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO subject_student (sub_id, stu_id, mod_by, mod_date) VALUES (?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, stu_id);
			ps.setNull(3, java.sql.Types.INTEGER);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectStudentSubject(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu.sub_stu_id , sub_stu.sub_id , sub.sub_name , sub.sub_co , sub_li.sec , sem.sem_co , sub_li.sem_id , sta.sta_name||'('||po.po_co||sub_stu.mod_by||')' , sub_stu.mod_date FROM subject_student sub_stu LEFT OUTER JOIN staff sta ON sub_stu.mod_by = sta.sta_id LEFT OUTER JOIN position po ON po.po_id = sta.sta_po LEFT OUTER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id LEFT OUTER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id LEFT OUTER JOIN subject sub ON sub.sub_co = sub_li.sub_co WHERE sub_stu.stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subStuId.clear();
			subId.clear();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				subStuId.add(rs.getInt(1));
				subId.add(rs.getInt(2));
				subName.add(rs.getNString(3));
				subCo.add(rs.getInt(4));
				sec.add(rs.getInt(5));
				semCo.add(rs.getNString(6));
				semId.add(rs.getInt(7));
				modBy.add(rs.getNString(8));
				modDate.add(rs.getTimestamp(9));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentSubjectBySubStuId(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu.sub_id , sub.sub_name , sub.sub_co , sub_li.sec , sem.sem_co FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id INNER JOIN subject sub ON sub.sub_co = sub_li.sub_co WHERE sub_stu.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				subId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				subCo.add(rs.getInt(3));
				sec.add(rs.getInt(4));
				semCo.add(rs.getNString(5));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean updateStudentSubject(Connection connection, int sub_id, int sub_stu_id) {
		// TODO Auto-generated method stub
		databaseConnection(); 
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE subject_student SET sub_id = ?, mod_by = ?, mod_date = ? WHERE sub_stu_id= ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setNull(2, java.sql.Types.INTEGER);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(4, sub_stu_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail update data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteStudentSubject(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM subject_student WHERE sub_stu_id= ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_stu_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public String selectSemCode(Connection connection, int sem_id) {
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem_co FROM sem_date WHERE sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sem_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getNString(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public String selectSubjectName(Connection connection, int sub_stu_id) {
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub.sub_name FROM subject sub INNER JOIN subject_list sub_li ON sub.sub_co = sub_li.sub_co INNER JOIN subject_student sub_stu ON sub_li.sub_id = sub_stu.sub_id WHERE sub_stu.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getNString(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public void selectStudentEvaluationWholeStudy(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu.sub_stu_id , sub.sub_name , sub_li.sec , sem.sem_co , sem.sem_id , total_mark.total FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_stu.sub_id = sub_li.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id INNER JOIN (SELECT sub_stu_id, SUM(marks) AS total FROM student_evaluate GROUP BY sub_stu_id) total_mark ON total_mark.sub_stu_id = sub_stu.sub_stu_id WHERE sub_stu.stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subStuId.clear();
			subName.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			marks.clear();
			while(rs.next()) {
				subStuId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				sec.add(rs.getInt(3));
				semCo.add(rs.getNString(4));
				semId.add(rs.getInt(5));
				marks.add(rs.getFloat(6));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentEvaluationBySem(Connection connection, int stu_id, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu.sub_stu_id, sub.sub_name, sub_li.sec, total_mark.total FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_stu.sub_id = sub_li.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN (SELECT sub_stu_id, SUM(marks) AS total FROM student_evaluate GROUP BY sub_stu_id) total_mark ON total_mark.sub_stu_id = sub_stu.sub_stu_id WHERE sub_stu.stu_id = ? AND sub_li.sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			ps.setInt(2, sem_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subStuId.clear();
			subName.clear();
			sec.clear();
			marks.clear();
			while(rs.next()) {
				subStuId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				sec.add(rs.getInt(3));
				marks.add(rs.getFloat(4));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentEvaluationByCourse(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT ev_me.me_name, stu_ev.marks, sta.sta_name||'('||po.po_co||stu_ev.mod_by||')', stu_ev.mod_date FROM student_evaluate stu_ev INNER JOIN course_evaluate course_ev ON stu_ev.ev_id = course_ev.ev_id INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id INNER JOIN staff sta ON stu_ev.mod_by = sta.sta_id INNER JOIN position po ON po.po_id = sta.sta_id WHERE stu_ev.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			totalMark = 0;
			meName.clear();
			marks.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				meName.add(rs.getString(1));
				marks.add(rs.getFloat(2));
				modBy.add(rs.getNString(3));
				modDate.add(rs.getTimestamp(4));
				totalMark += rs.getFloat(2);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectSemInStudentEvaluation(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem_co, sem_id FROM sem_date sem WHERE sem_id IN (SELECT sem.sem_id FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_stu.sub_id = sub_li.sub_id INNER JOIN sem_date sem ON sem.sem_id = sub_li.sem_id WHERE sub_stu.stu_id = ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			semCo.clear();
			semId.clear();
			while(rs.next()) {
				semCo.add(rs.getNString(1));
				semId.add(rs.getInt(2));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
}
