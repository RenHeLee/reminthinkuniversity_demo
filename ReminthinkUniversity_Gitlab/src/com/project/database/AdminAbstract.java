package com.project.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.sun.swing.internal.plaf.metal.resources.metal_it;

public abstract class AdminAbstract extends LecturerAbstract implements StudentInterface, AdminInterface{

	protected ArrayList<String> inSemCo = new ArrayList<String>();
	protected ArrayList<Integer> inSemId = new ArrayList<Integer>();
	protected ArrayList<String> graSemCo = new ArrayList<String>();
	protected ArrayList<Integer> graSemId = new ArrayList<Integer>();
	protected ArrayList<String> phoneNumber = new ArrayList<String>();
	protected ArrayList<String> addressInfo = new ArrayList<String>();
	protected ArrayList<String> icNumber = new ArrayList<String>();
	protected ArrayList<String> code = new ArrayList<String>();
	protected ArrayList<String> poCo = new ArrayList<String>();
	protected ArrayList<Integer> poId = new ArrayList<Integer>();
	protected ArrayList<String> poTitle = new ArrayList<String>();
	protected ArrayList<Date> firDate  = new ArrayList<Date>();
	protected ArrayList<Date> lasDate  = new ArrayList<Date>();
	protected ArrayList<Integer> woId = new ArrayList<Integer>();
	protected ArrayList<Integer> meId = new ArrayList<Integer>();
	
	
	public ArrayList<String> getInSemCo() {
		return inSemCo;
	}

	public ArrayList<Integer> getInSemId() {
		return inSemId;
	}

	public ArrayList<String> getGraSemCo() {
		return graSemCo;
	}

	public ArrayList<Integer> getGraSemId() {
		return graSemId;
	}

	public ArrayList<String> getPhoneNumber() {
		return phoneNumber;
	}

	public ArrayList<String> getAddressInfo() {
		return addressInfo;
	}

	public ArrayList<String> getIcNumber() {
		return icNumber;
	}

	public ArrayList<String> getCode() {
		return code;
	}

	public ArrayList<String> getPoCo() {
		return poCo;
	}

	public ArrayList<Integer> getPoId() {
		return poId;
	}

	public ArrayList<String> getPoTitle() {
		return poTitle;
	}

	public ArrayList<Date> getFirDate() {
		return firDate;
	}

	public ArrayList<Date> getLasDate() {
		return lasDate;
	}

	public ArrayList<Integer> getWoId() {
		return woId;
	}

	public ArrayList<Integer> getMeId() {
		return meId;
	}


	@Override
	public void selectAllStaffInfo(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT po.po_co, sta.sta_id, sta.sta_name, po.po_title, sta.sta_po, sta.fir_ser_date, sta.las_ser_date, sta.pho_num, sta.ic_num, sta.address, modified.sta_name||'('||mod_po.po_co||modified.sta_id||')', sta.mod_date FROM staff sta INNER JOIN position po ON sta.sta_po = po.po_id INNER JOIN staff modified ON modified.sta_id = sta.mod_by INNER JOIN position mod_po ON modified.sta_po = mod_po.po_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			poCo.clear();
			idList.clear();
			nameList.clear();
			poTitle.clear();
			poId.clear();
			firDate.clear();
			lasDate.clear();
			phoneNumber.clear();
			icNumber.clear();
			addressInfo.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				poCo.add(rs.getNString(1));
				idList.add(rs.getInt(2));
				nameList.add(rs.getNString(3));
				poTitle.add(rs.getNString(4));
				poId.add(rs.getInt(5));
				firDate.add(rs.getDate(6));
				lasDate.add(rs.getDate(7));
				phoneNumber.add(rs.getNString(8));
				icNumber.add(rs.getNString(9));
				addressInfo.add(rs.getNString(10));
				modBy.add(rs.getNString(11));
				modDate.add(rs.getTimestamp(12));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateStaffInfo(Connection connection, String sta_name, int sta_po, Date fir_ser_date, Date las_ser_date, String pho_num, String ic_num, String address, int mod_by, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE staff SET sta_name = ?, sta_po = ?, fir_ser_date = ?, las_ser_date = ?, pho_num = ?, ic_num = ?, address = ?, mod_by = ?, mod_date = ? WHERE sta_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, sta_name);
			ps.setInt(2, sta_po);
			ps.setDate(3, new java.sql.Date(fir_ser_date.getTime()));
			ps.setDate(4, new java.sql.Date(las_ser_date.getTime()));
			ps.setNString(5, pho_num);
			ps.setString(6, ic_num);
			ps.setNString(7, address);
			ps.setInt(8, mod_by);
			ps.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(10, sta_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail update data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkStaffInfo(Connection connection, String ic_num) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sta_id FROM staff WHERE ic_num = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, ic_num);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertStaffInfo(Connection connection, String sta_name, int sta_po, Date fir_ser_date, Date las_ser_date, String pho_num, String ic_num, String address, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO staff (sta_name, sta_password, sta_po, fir_ser_date, las_ser_date, pho_num, ic_num, address, mod_by, mod_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, sta_name);
			ps.setNString(2, AES.encrypt(ic_num));
			ps.setInt(3, sta_po);
			ps.setDate(4, new java.sql.Date(fir_ser_date.getTime()));
			ps.setDate(5, new java.sql.Date(las_ser_date.getTime()));
			ps.setNString(6, pho_num);
			ps.setString(7, ic_num);
			ps.setNString(8, address);
			ps.setInt(9, mod_by);
			ps.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteStaffInfo(Connection connection, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM staff WHERE sta_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sta_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectSemInWorkload(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem.sem_co, wl.sem_id FROM (SELECT DISTINCT sem_id FROM subject_list WHERE sub_id IN (SELECT sub_id FROM workload)) wl INNER JOIN sem_date sem ON wl.sem_id = sem.sem_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sta_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			semCo.clear();
			semId.clear();
			while(rs.next()) {
				semCo.add(rs.getNString(1));
				semId.add(rs.getInt(2));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectAllWorkload(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT wl.wl_id, sem.sem_co, sub_li.sem_id, sub.sub_name, sub_li.sec, wl.sub_id, sta.sta_name||'('||sta_po.po_co||wl.sta_id||')', wl.sta_id, mod.sta_name||'('||mod_po.po_co||wl.mod_by||')', wl.mod_date FROM workload wl INNER JOIN staff sta ON wl.sta_id = sta.sta_id INNER JOIN position sta_po ON sta.sta_po = sta_po.po_id INNER JOIN subject_list sub_li ON sub_li.sub_id = wl.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN staff mod ON wl.mod_by = mod.sta_id INNER JOIN position mod_po ON mod.sta_po = mod_po.po_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			woId.clear();
			semCo.clear();
			semId.clear();
			subName.clear();
			sec.clear();
			subId.clear();
			nameList.clear();
			idList.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				woId.add(rs.getInt(1));
				semCo.add(rs.getNString(2));
				semId.add(rs.getInt(3));
				subName.add(rs.getNString(4));
				sec.add(rs.getInt(5));
				subId.add(rs.getInt(6));
				nameList.add(rs.getNString(7));
				idList.add(rs.getInt(8));
				modBy.add(rs.getNString(9));
				modDate.add(rs.getTimestamp(10));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectWorkload(Connection connection, int wo_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT wl.wl_id, sem.sem_co, sub_li.sem_id, sub.sub_name, sub_li.sec, wl.sub_id, sta.sta_name||'('||sta_po.po_co||wl.sta_id||')', wl.sta_id, mod.sta_name||'('||mod_po.po_co||wl.mod_by||')', wl.mod_date FROM workload wl INNER JOIN staff sta ON wl.sta_id = sta.sta_id INNER JOIN position sta_po ON sta.sta_po = sta_po.po_id INNER JOIN subject_list sub_li ON sub_li.sub_id = wl.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN staff mod ON wl.mod_by = mod.sta_id INNER JOIN position mod_po ON mod.sta_po = mod_po.po_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id WHERE wl.wl_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, wo_id);
			ResultSet rs = ps.executeQuery();
			woId.clear();
			semCo.clear();
			semId.clear();
			subName.clear();
			sec.clear();
			subId.clear();
			nameList.clear();
			idList.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				woId.add(rs.getInt(1));
				semCo.add(rs.getNString(2));
				semId.add(rs.getInt(3));
				subName.add(rs.getNString(4));
				sec.add(rs.getInt(5));
				subId.add(rs.getInt(6));
				nameList.add(rs.getNString(7));
				idList.add(rs.getInt(8));
				modBy.add(rs.getNString(9));
				modDate.add(rs.getTimestamp(10));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateWorkload(Connection connection, int sub_id, int sta_id, int mod_by, int wl_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE workload SET sub_id = ?, sta_id = ?, mod_by = ?, mod_date = ? WHERE wl_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, sta_id);
			ps.setInt(3, mod_by);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(5, wl_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkWorkload(Connection connection, int sub_id, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT wl_id FROM workload WHERE sub_id = ? AND sta_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, sta_id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertWorkload(Connection connection, int sub_id, int sta_id, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO workload (sub_id, sta_id, mod_by, mod_date) VALUES (?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, sta_id);
			ps.setInt(3, mod_by);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteWorkload(Connection connection, int wl_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM workload WHERE wl_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, wl_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectAllCourseEvaluate(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT course_ev.ev_id, ev_me.me_name, ev_me.me_id, sub.sub_name, sub.sub_co, sub_li.sub_id, sub_li.sec, course_ev.ev_weightage, sta.sta_name||'('||po.po_co||course_ev.mod_by||')', course_ev.mod_date FROM course_evaluate course_ev INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id INNER JOIN subject_list sub_li ON course_ev.sub_id = sub_li.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN staff sta ON course_ev.mod_by = sta.sta_id INNER JOIN position po ON po.po_id = sta.sta_po";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			evId.clear();
			meName.clear();
			meId.clear();
			subName.clear();
			subCo.clear();
			subId.clear();
			sec.clear();
			evWeightage.clear();
			modBy.clear();
			modDate.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				evId.add(rs.getInt(1));
				meName.add(rs.getNString(2));
				meId.add(rs.getInt(3));
				subName.add(rs.getNString(4));
				subCo.add(rs.getInt(5));
				subId.add(rs.getInt(6));
				sec.add(rs.getInt(7));
				evWeightage.add(rs.getFloat(8));
				modBy.add(rs.getNString(9));
				modDate.add(rs.getTimestamp(10));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectCourseEvaluate(int ev_id, Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT ev_me.me_name, ev_me.me_id, sub.sub_name, sub.sub_co, sub_li.sub_id, sub_li.sec, course_ev.ev_weightage FROM course_evaluate course_ev INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id INNER JOIN subject_list sub_li ON course_ev.sub_id = sub_li.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co WHERE course_ev.ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			meName.clear();
			meId.clear();
			subName.clear();
			subCo.clear();
			subId.clear();
			sec.clear();
			evWeightage.clear();
			ps.setInt(1, ev_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				meName.add(rs.getNString(1));
				meId.add(rs.getInt(2));
				subName.add(rs.getNString(3));
				subCo.add(rs.getInt(4));
				subId.add(rs.getInt(5));
				sec.add(rs.getInt(6));
				evWeightage.add(rs.getFloat(7));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectCourseEvaluate(Connection connection, int sub_co) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT course_ev.ev_id, ev_me.me_name, ev_me.me_id, sub_li.sub_id, sub_li.sec, course_ev.ev_weightage, sta.sta_name||'('||po.po_co||course_ev.mod_by||')', course_ev.mod_date FROM course_evaluate course_ev INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id INNER JOIN subject_list sub_li ON course_ev.sub_id = sub_li.sub_id INNER JOIN staff sta ON course_ev.mod_by = sta.sta_id INNER JOIN position po ON po.po_id = sta.sta_po WHERE sub_li.sub_co = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_co);
			//get query execute status
			evId.clear();
			meName.clear();
			meId.clear();
			subId.clear();
			sec.clear();
			evWeightage.clear();
			modBy.clear();
			modDate.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				evId.add(rs.getInt(1));
				meName.add(rs.getNString(2));
				meId.add(rs.getInt(3));
				subId.add(rs.getInt(4));
				sec.add(rs.getInt(5));
				evWeightage.add(rs.getFloat(6));
				modBy.add(rs.getNString(7));
				modDate.add(rs.getTimestamp(8));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateCourseEvaluate(Connection connection, int sub_id, int me_id, float ev_weightage, int mod_by, int ev_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE course_evaluate SET sub_id = ?, me_id = ?, ev_weightage = ?, mod_by = ?, mod_date = ? WHERE ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, me_id);
			ps.setFloat(3, ev_weightage);
			ps.setInt(4, mod_by);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(6, ev_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail update data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkCourseEvaluate(Connection connection, int sub_id, int me_id, float ev_weightage) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT ev_id FROM course_evaluate WHERE sub_id = ? AND me_id = ? AND ev_weightage = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, me_id);
			ps.setFloat(3, ev_weightage);
			rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertCourseEvaluate(Connection connection, int sub_id, int me_id, float ev_weightage, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO course_evaluate (sub_id, me_id, ev_weightage, mod_by, mod_date) VALUES (?, ?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, me_id);
			ps.setFloat(3, ev_weightage);
			ps.setInt(4, mod_by);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteCourseEvaluate(Connection connection, int ev_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM course_evaluate WHERE ev_id = ?" ;
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, ev_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public void selectAllSubject(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub.sub_co, sub.sub_name, sta.sta_name||'('||po.po_co||sub.mod_by||')', sub.mod_date FROM subject sub INNER JOIN staff sta ON sub.mod_by = sta.sta_id INNER JOIN position po ON sta.sta_po = po.po_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subCo.clear();
			subName.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				subCo.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				modBy.add(rs.getNString(3));
				modDate.add(rs.getTimestamp(4));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectSubject(Connection connection, int sub_co) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_name FROM subject WHERE sub_co = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_co);
			ResultSet rs = ps.executeQuery();
			subName.clear();
			while(rs.next()) {
				subName.add(rs.getNString(1));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateSubject(Connection connection, String sub_name, int mod_by, int sub_co) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE subject SET sub_name = ?, mod_by = ?, mod_date = ? WHERE sub_co = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, sub_name);
			ps.setInt(2, mod_by);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(4, sub_co);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkSubject(Connection connection, String sub_name) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_co FROM subject WHERE sub_name = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, sub_name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertSubject(Connection connection, String sub_name, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO subject (sub_name, mod_by, mod_date) VALUES (?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, sub_name);
			ps.setInt(2, mod_by);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteSubject(Connection connection, int sub_co) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM subject WHERE sub_co = ?" ;
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_co);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectAllSubjectBySection(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_li.sub_id, sub.sub_name, sub_li.sub_co, sub_li.sec, sem.sem_co, sub_li.sem_id, sta.sta_name||'('||po.po_co||sub_li.mod_by||')', sub_li.mod_date FROM subject_list sub_li INNER JOIN staff sta ON sub_li.mod_by = sta.sta_id INNER JOIN position po ON sta.sta_po = po.po_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN sem_date sem ON sem.sem_id = sub_li.sem_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subId.clear();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				subId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				subCo.add(rs.getInt(3));
				sec.add(rs.getInt(4));
				semCo.add(rs.getNString(5));
				semId.add(rs.getInt(6));
				modBy.add(rs.getNString(7));
				modDate.add(rs.getTimestamp(8));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectSubjectBySection(Connection connection, int sub_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub.sub_name, sub_li.sub_co, sub_li.sec, sem.sem_co, sub_li.sem_id FROM subject_list sub_li INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN sem_date sem ON sem.sem_id = sub_li.sem_id WHERE sub_li.sub_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ResultSet rs = ps.executeQuery();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();
			semId.clear();;
			while(rs.next()) {
				subName.add(rs.getNString(1));
				subCo.add(rs.getInt(2));
				sec.add(rs.getInt(3));
				semCo.add(rs.getNString(4));
				semId.add(rs.getInt(5));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateSubjectBySection(Connection connection, int sub_co, int sec, int sem_id, int mod_by, int sub_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE subject_list SET sub_co = ?, sec = ?, sem_id = ?, mod_by = ?, mod_date = ? WHERE sub_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_co);
			ps.setInt(2, sec);
			ps.setInt(3, sem_id);
			ps.setInt(4, mod_by);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(6, sub_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkSubjectBySection(Connection connection, int sub_co, int sec, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_id FROM subject_list WHERE sub_co = ? AND sec = ? AND sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_co);
			ps.setInt(2, sec);
			ps.setInt(3, sem_id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertSubjectBySection(Connection connection, int sub_co, int sec, int sem_id, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO subject_list (sub_co, sec, sem_id, mod_by, mod_date) VALUES (?, ?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_co);
			ps.setInt(2, sec);
			ps.setInt(3, sem_id);
			ps.setInt(4, mod_by);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteSubjectBySection(Connection connection, int sub_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM subject_list WHERE sub_id = ?" ;
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public void selectAllSemDate(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem.sem_id, sem.in_date, sem.gra_date, sem.sem_co, sta.sta_name||'('||po.po_co||sem.mod_by||')', sem.mod_date FROM sem_date sem INNER JOIN staff sta ON sem.mod_by = sta.sta_id INNER JOIN position po ON sta.sta_po = po.po_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			semId.clear();
			firDate.clear();
			lasDate.clear();
			semCo.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				semId.add(rs.getInt(1));
				firDate.add(rs.getDate(2));
				lasDate.add(rs.getDate(3));
				semCo.add(rs.getNString(4));
				modBy.add(rs.getNString(5));
				modDate.add(rs.getTimestamp(6));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectSemDate(Connection connection, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT in_date, gra_date, sem_co FROM sem_date WHERE sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sem_id);
			ResultSet rs = ps.executeQuery();
			firDate.clear();
			lasDate.clear();
			semCo.clear();
			while(rs.next()) {
				firDate.add(rs.getDate(1));
				lasDate.add(rs.getDate(2));
				semCo.add(rs.getNString(3));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	
	@Override
	public boolean updateSemDate(Connection connection, Date in_date, Date gra_date, String sem_co, int mod_by, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE sem_date  SET in_date = ?, gra_date = ?, sem_co = ?, mod_by = ?, mod_date = ? WHERE sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setDate(1, new java.sql.Date(in_date.getTime()));
			ps.setDate(2, new java.sql.Date(gra_date.getTime()));
			ps.setNString(3, sem_co);
			ps.setInt(4, mod_by);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(6, sem_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkSemDate(Connection connection, String sem_co) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem_id FROM sem_date WHERE sem_co = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, sem_co);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertSemDate(Connection connection, Date in_date, Date gra_date, String sem_co, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO sem_date (in_date, gra_date, sem_co, mod_by, mod_date) VALUES (?, ?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setDate(1, new java.sql.Date(in_date.getTime()));
			ps.setDate(2, new java.sql.Date(gra_date.getTime()));
			ps.setNString(3, sem_co);
			ps.setInt(4, mod_by);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteSemDate(Connection connection, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM sem_date WHERE sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sem_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectAllEvaluateMethod(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT me.me_id, me.me_name, sta.sta_name||'('||po.po_co||me.mod_by||')', me.mod_date FROM evaluate_method me INNER JOIN staff sta ON me.mod_by = sta.sta_id INNER JOIN position po ON sta.sta_po = po.po_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			meId.clear();
			meName.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				meId.add(rs.getInt(1));
				meName.add(rs.getNString(2));
				modBy.add(rs.getNString(3));
				modDate.add(rs.getTimestamp(4));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectAllEvaluateMethod(Connection connection, int me_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT me_name FROM evaluate_method WHERE me_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, me_id);
			ResultSet rs = ps.executeQuery();
			meName.clear();
			while(rs.next()) {
				meName.add(rs.getNString(1));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean updateEvaluateMethod(Connection connection, String me_name, int mod_by, int me_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE evaluate_method SET me_name = ?, mod_by = ?, mod_date = ? WHERE me_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, me_name);
			ps.setInt(2, mod_by);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(4, me_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkEvaluateMethod(Connection connection, String me_name) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT me_id FROM evaluate_method WHERE me_name = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, me_name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertEvaluateMethod(Connection connection, String me_name, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO evaluate_method (me_name, mod_by, mod_date) VALUES (?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, me_name);
			ps.setInt(2, mod_by);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteEvaluateMethod(Connection connection, int me_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM evaluate_method WHERE me_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, me_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkStudentSubject(Connection connection, int sub_id, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu_id FROM subject_student WHERE sub_id = ? AND stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, stu_id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}



	@Override
	public void selectSemInStudentEvaluation(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT DISTINCT sem.sem_co, sub_li.sem_id FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			semCo.clear();
			semId.clear();
			while(rs.next()) {
				semCo.add(rs.getNString(1));
				semId.add(rs.getInt(2));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectSemInStudentEvaluation(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT DISTINCT sem.sem_co, sub_li.sem_id FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id WHERE sub_stu.stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			semCo.clear();
			semId.clear();
			while(rs.next()) {
				semCo.add(rs.getNString(1));
				semId.add(rs.getInt(2));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentEvaluationWholeStudy(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu.sub_stu_id , sub.sub_name , sub_li.sec , sem.sem_co , sem.sem_id , total_mark.total FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_stu.sub_id = sub_li.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id INNER JOIN (SELECT sub_stu_id, SUM(marks) AS total FROM student_evaluate GROUP BY sub_stu_id) total_mark ON total_mark.sub_stu_id = sub_stu.sub_stu_id WHERE sub_stu.stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subStuId.clear();
			subName.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			marks.clear();
			while(rs.next()) {
				subStuId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				sec.add(rs.getInt(3));
				semCo.add(rs.getNString(4));
				semId.add(rs.getInt(5));
				marks.add(rs.getFloat(6));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentEvaluationBySem(Connection connection, int stu_id, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_stu.sub_stu_id, sub.sub_name, sub_li.sec, total_mark.total FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_stu.sub_id = sub_li.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN (SELECT sub_stu_id, SUM(marks) AS total FROM student_evaluate GROUP BY sub_stu_id) total_mark ON total_mark.sub_stu_id = sub_stu.sub_stu_id WHERE sub_stu.stu_id = ? AND sub_li.sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			ps.setInt(2, sem_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			subStuId.clear();
			subName.clear();
			sec.clear();
			marks.clear();
			while(rs.next()) {
				subStuId.add(rs.getInt(1));
				subName.add(rs.getNString(2));
				sec.add(rs.getInt(3));
				marks.add(rs.getFloat(4));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentEvaluationByCourse(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT ev_me.me_name, stu_ev.marks, sta.sta_name||'('||po.po_co||stu_ev.mod_by||')', stu_ev.mod_date FROM student_evaluate stu_ev INNER JOIN course_evaluate course_ev ON stu_ev.ev_id = course_ev.ev_id INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id INNER JOIN staff sta ON stu_ev.mod_by = sta.sta_id INNER JOIN position po ON po.po_id = sta.sta_id WHERE stu_ev.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			totalMark = 0;
			meName.clear();
			marks.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				meName.add(rs.getString(1));
				marks.add(rs.getFloat(2));
				modBy.add(rs.getNString(3));
				modDate.add(rs.getTimestamp(4));
				totalMark += rs.getFloat(2);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectAllPosition(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT po.po_id, po.po_co, po.po_title,sta.sta_name||'('||mod.po_co||po.mod_by||')', po.mod_date FROM position po INNER JOIN staff sta ON po.mod_by = sta.sta_id INNER JOIN position mod  ON sta.sta_po = mod.po_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			poId.clear();
			poCo.clear();
			poTitle.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				poId.add(rs.getInt(1));
				poCo.add(rs.getNString(2));
				poTitle.add(rs.getNString(3));
				modBy.add(rs.getNString(4));
				modDate.add(rs.getTimestamp(5));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updatePosition(Connection connection, String po_co, String po_title, int mod_by, int po_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE position SET po_co = ?, po_title = ?, mod_by = ?, mod_date = ? WHERE po_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, po_co);
			ps.setNString(2, po_title);
			ps.setInt(3, mod_by);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(5, po_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkPosition(Connection connection, String po_co, String po_title) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT po_id FROM position WHERE po_co = ? AND po_title = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, po_co);
			ps.setNString(2, po_title);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertPosition(Connection connection, String po_co, String po_title, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO position (po_co, po_title, mod_by, mod_date) VALUES (?, ?, ?, ?)";
			//Initialize value for column
			ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, po_co);
			ps.setNString(2, po_title);
			ps.setInt(3, mod_by);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deletePosition(Connection connection, int po_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM position WHERE po_id = ?" ;
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, po_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}
	
	
	
}
