package com.project.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


public class User {

	private String username = "Error";
	private String password;
	private String decryptedPassword;
	private Date validLoginDate;
	private int position = 9;
	private int privilege = 9;
	private Date nowDate = new Date();
	private int userId;

	public Connection connection = null;


	protected ResultSet rs;
	protected PreparedStatement ps;


	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDecryptedPassword() {
		return decryptedPassword;
	}

	public void setDecryptedPassword(String decryptedPassword) {
		this.decryptedPassword = decryptedPassword;
	}

	public Date getValidLoginDate() {
		return validLoginDate;
	}

	public void setValidLoginDate(Date validLoginDate) {
		this.validLoginDate = validLoginDate;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getPrivilege() {
		return privilege;
	}

	public void setPrivilege(int privilege) {
		this.privilege = privilege;
	}

	public ResultSet getRs() {
		return rs;
	}

	public void setRs(ResultSet rs) {
		this.rs = rs;
	}

	public Date getNowDate() {
		return nowDate;
	}

	public void setNowDate(Date nowDate) {
		this.nowDate = nowDate;
	}


	public Connection databaseConnection() {
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
		}catch(ClassNotFoundException e){
			System.out.println("Error occured when JDBC is added, as below: ");
			e.printStackTrace();
		}

		try {
			// Create a connection to the database
			String serverName = "localhost";
			String portNumber = "1521";
			String sid = "xe";

			String serverUsername = "system";
			String serverPassword = "1234";
			String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid;
			connection = DriverManager.getConnection(url, serverUsername, serverPassword);
			return connection;
		}catch (SQLException e) {
			//Could not connect to the database
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return connection;
		}

	}

	public boolean login (int privilege, int id, String password) {
		// privilege = 0 for admin table
		Connection connection = databaseConnection();
		System.out.println(connection);
		if (privilege == 0){
			try {
				//prepare sql statement with dynamic value
				String sql = "SELECT sta_name, las_ser_date, sta_po FROM staff WHERE sta_id = ? AND sta_password = ?";
				//Initialize value for column
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, id);
				ps.setNString(2, AES.encrypt(password));
				//get query execute status
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					username = rs.getNString(1);
					validLoginDate = rs.getDate(2);
					position = rs.getInt(3);
					return true;
				}
			} catch (SQLException e) {
				System.out.println("Fail retrive data");
				e.printStackTrace();
			}
		}else if (privilege == 1) {
			try {
				//prepare sql statement with dynamic value
				String sql = "SELECT stu.stu_name, sem.gra_date FROM student stu INNER JOIN sem_date sem ON stu.gra_sem_id = sem.sem_id WHERE stu.stu_id = ? AND stu.stu_password = ?";
				//Initialize value for column
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, id);
				ps.setNString(2, AES.encrypt(password));
				//get query execute status
				rs = ps.executeQuery();
				while(rs.next()) {
					username = rs.getNString(1);
					validLoginDate = rs.getDate(2);
					position = 3;
				}
				return true;
			} catch (SQLException e) {
				System.out.println("Fail retrive data");
				e.printStackTrace();
			}
		}
		return false;
	}
}
