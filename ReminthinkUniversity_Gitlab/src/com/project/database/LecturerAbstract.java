package com.project.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import sun.swing.SwingUtilities2.Section;


public abstract class LecturerAbstract extends User implements LecturerInterface{

	protected int sta_id = getUserId();
	protected int id;
	protected String po_co;
	protected String name;
	protected String po_title;
	protected Date fir_ser_date;
	protected Date las_ser_date;
	protected String intake_sem_co;
	protected int intake_sem_id;
	protected String graduate_sem_co;
	protected int graduate_sem_id;
	protected String phone;
	protected String ic;
	protected String address;
	protected String mod_by;
	protected Date mod_date;
	protected float totalMark;
	protected int sub_id;
	protected int po_id;
	protected String sub_name;
	protected int sub_co;
	protected int section;
	protected String sem_co;
	
	
	protected ArrayList<String> inSemCo = new ArrayList<String>();
	protected ArrayList<Integer> inSemId = new ArrayList<Integer>();
	protected ArrayList<String> graSemCo = new ArrayList<String>();
	protected ArrayList<Integer> graSemId = new ArrayList<Integer>();
	protected ArrayList<String> phoneNumber = new ArrayList<String>();
	protected ArrayList<String> addressInfo = new ArrayList<String>();
	protected ArrayList<String> icNumber = new ArrayList<String>();
	protected ArrayList<String> poCo = new ArrayList<String>();
	protected ArrayList<String> semCo = new ArrayList<String>();
	protected ArrayList<Integer> semId = new ArrayList<Integer>();
	protected ArrayList<String> subName = new ArrayList<String>();
	protected ArrayList<Integer> sec = new ArrayList<Integer>();
	protected ArrayList<Integer> subId = new ArrayList<Integer>();
	protected ArrayList<String> modBy = new ArrayList<String>();
	protected ArrayList<Date> modDate  = new ArrayList<Date>();
	protected ArrayList<Integer> idList = new ArrayList<Integer>();
	protected ArrayList<Integer> subCo = new ArrayList<Integer>();
	protected ArrayList<String> nameList = new ArrayList<String>();
	protected ArrayList<Integer> subStuId = new ArrayList<Integer>();
	protected ArrayList<String> meName = new ArrayList<String>();
	protected ArrayList<Float> marks = new ArrayList<Float>();
	protected ArrayList<Integer> evId = new ArrayList<Integer>();
	protected ArrayList<Float> evWeightage = new ArrayList<Float>();
	protected ArrayList<Float> total = new ArrayList<Float>();
	protected ArrayList<Integer> stuEvId = new ArrayList<Integer>();
	protected ArrayList<Integer> woId = new ArrayList<Integer>();
	
	
	public int getPo_id() {
		return po_id;
	}

	public int getSub_id() {
		return sub_id;
	}

	public int getSta_id() {
		return sta_id;
	}

	public int getId() {
		return id;
	}

	public String getPo_co() {
		return po_co;
	}

	public String getName() {
		return name;
	}

	public String getPo_title() {
		return po_title;
	}

	public Date getFir_ser_date() {
		return fir_ser_date;
	}

	public Date getLas_ser_date() {
		return las_ser_date;
	}

	public String getIntake_sem_co() {
		return intake_sem_co;
	}

	public int getIntake_sem_id() {
		return intake_sem_id;
	}

	public String getGraduate_sem_co() {
		return graduate_sem_co;
	}

	public int getGraduate_sem_id() {
		return graduate_sem_id;
	}

	public String getPhone() {
		return phone;
	}

	public String getIc() {
		return ic;
	}

	public String getAddress() {
		return address;
	}

	public String getMod_by() {
		return mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public float getTotalMark() {
		return totalMark;
	}

	public ArrayList<String> getInSemCo() {
		return inSemCo;
	}

	public ArrayList<Integer> getInSemId() {
		return inSemId;
	}

	public ArrayList<String> getGraSemCo() {
		return graSemCo;
	}

	public ArrayList<Integer> getGraSemId() {
		return graSemId;
	}

	public ArrayList<String> getPhoneNumber() {
		return phoneNumber;
	}

	public ArrayList<String> getAddressInfo() {
		return addressInfo;
	}

	public ArrayList<String> getIcNumber() {
		return icNumber;
	}

	public ArrayList<String> getPoCo() {
		return poCo;
	}

	public ArrayList<String> getSemCo() {
		return semCo;
	}

	public ArrayList<Integer> getSemId() {
		return semId;
	}

	public ArrayList<String> getSubName() {
		return subName;
	}

	public ArrayList<Integer> getSec() {
		return sec;
	}

	public ArrayList<Integer> getSubId() {
		return subId;
	}

	public String getSub_name() {
		return sub_name;
	}

	public int getSub_co() {
		return sub_co;
	}

	public int getSection() {
		return section;
	}

	public String getSem_co() {
		return sem_co;
	}

	public ArrayList<String> getModBy() {
		return modBy;
	}

	public ArrayList<Date> getModDate() {
		return modDate;
	}

	public ArrayList<Integer> getIdList() {
		return idList;
	}

	public ArrayList<Integer> getSubCo() {
		return subCo;
	}

	public ArrayList<String> getNameList() {
		return nameList;
	}

	public ArrayList<Integer> getSubStuId() {
		return subStuId;
	}

	public ArrayList<String> getMeName() {
		return meName;
	}

	public ArrayList<Float> getMarks() {
		return marks;
	}

	public ArrayList<Integer> getEvId() {
		return evId;
	}

	public ArrayList<Float> getEvWeightage() {
		return evWeightage;
	}

	public ArrayList<Float> getTotal() {
		return total;
	}

	public ArrayList<Integer> getStuEvId() {
		return stuEvId;
	}

	public ArrayList<Integer> getWoId() {
		return woId;
	}

	@Override
	public void selectStaffInfo(Connection connection, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT po.po_co , sta.sta_name , po.po_title , sta.sta_po , sta.fir_ser_date , sta.las_ser_date , sta.pho_num , sta.ic_num , sta.address , mod.sta_name||'('||mod_po.po_co||mod.sta_id||')' , sta.mod_date FROM staff sta INNER JOIN position po ON sta.sta_po = po.po_id INNER JOIN staff mod ON mod.sta_id = sta.mod_by INNER JOIN position mod_po ON mod.sta_po = mod_po.po_id WHERE sta.sta_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sta_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				po_co = rs.getNString(1);
				name = rs.getNString(2);
				po_title = rs.getNString(3);
				po_id = rs.getInt(4);
				fir_ser_date = rs.getDate(5);
				las_ser_date = rs.getDate(6);
				phone = rs.getNString(7);
				ic = rs.getNString(8);
				address = rs.getNString(9);
				mod_by = rs.getNString(10);
				mod_date = rs.getTimestamp(11);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateStaffInfo(Connection connection, int sta_id, String pho_num, String address) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE staff SET pho_num = ?, address = ?, mod_by = ?, mod_date = ? WHERE sta_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setNString(1, pho_num);
			ps.setNString(2, address);
			ps.setInt(3, sta_id);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(5, sta_id);
			//get query execute status
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectWorkload(Connection connection, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem.sem_co, sub_li.sem_id, sub.sub_name, sub_li.sec, wl.sub_id, mod.sta_name||'('||mod_po.po_co||wl.mod_by||')' , wl.mod_date, wl.wl_id FROM workload wl INNER JOIN subject_list sub_li ON sub_li.sub_id = wl.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN staff mod ON wl.mod_by = mod.sta_id INNER JOIN position mod_po ON mod.sta_po = mod_po.po_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id WHERE wl.sta_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sta_id);
			//get query execute status
			semCo.clear();
			semId.clear();
			subName.clear();
			sec.clear();
			subId.clear();
			modBy.clear();
			modDate .clear();
			woId.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				semCo.add(rs.getNString(1));
				semId.add(rs.getInt(2));
				subName.add(rs.getNString(3));
				sec.add(rs.getInt(4));
				subId.add(rs.getInt(5));
				modBy.add(rs.getNString(6));
				modDate.add(rs.getTimestamp(7));
				woId.add(rs.getInt(8)); 
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectWorkload(Connection connection, int sta_id, int sem_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub.sub_name, sub_li.sec, wl.sub_id, mod.sta_name||'('||mod_po.po_co||wl.mod_by||')', wl.mod_date, wl.wl_id FROM workload wl INNER JOIN subject_list sub_li ON sub_li.sub_id = wl.sub_id INNER JOIN subject sub ON sub_li.sub_co = sub.sub_co INNER JOIN staff mod ON wl.mod_by = mod.sta_id INNER JOIN position mod_po ON mod.sta_po = mod_po.po_id WHERE wl.sta_id = ? AND sub_li.sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sta_id);
			ps.setInt(2, sem_id);
			//get query execute status
			subName.clear();
			sec.clear();
			subId.clear();
			modBy.clear();
			modDate .clear();
			woId.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				subName.add(rs.getNString(1));
				sec.add(rs.getInt(2));
				subId.add(rs.getInt(3));
				modBy.add(rs.getNString(4));
				modDate.add(rs.getTimestamp(5)); 
				woId.add(rs.getInt(6)); 
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectSemInWorkload(Connection connection, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem.sem_co, wl.sem_id FROM (SELECT DISTINCT sem_id FROM subject_list WHERE sub_id IN (SELECT sub_id FROM workload WHERE sta_id = ?)) wl INNER JOIN sem_date sem ON wl.sem_id = sem.sem_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sta_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			semCo.clear();
			semId.clear();
			while(rs.next()) {
				semCo.add(rs.getNString(1));
				semId.add(rs.getInt(2));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectStudentInfo(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		mod_by = "a";
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT mod_by FROM student WHERE stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
			mod_by = rs.getNString(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		
		if (mod_by == null) {
			try {
				//prepare sql statement with dynamic value
				String sql = "SELECT po.po_co, stu.stu_name, intake.sem_co, stu.in_sem_id, gra.sem_co, stu.gra_sem_id, stu.pho_num, stu.ic_num, stu.address, stu_id, stu.mod_date FROM student stu INNER JOIN position po ON stu.stu_po = po.po_id  INNER JOIN sem_date intake ON stu.in_sem_id = intake.sem_id INNER JOIN sem_date gra ON stu.gra_sem_id = gra.sem_id WHERE stu.stu_id = ?";
				//Initialize value for column
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, stu_id);
				//get query execute status
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
				po_co = rs.getNString(1);
				name = rs.getNString(2);
				intake_sem_co = rs.getNString(3);
				intake_sem_id = rs.getInt(4);
				graduate_sem_co = rs.getNString(5);
				graduate_sem_id = rs.getInt(6);
				phone = rs.getNString(7);
				ic = rs.getNString(8);
				address = rs.getNString(9);
				mod_by = name + "(" + po_co + rs.getInt(10) + ")";
				mod_date = rs.getTimestamp(11);
				}
			} catch (SQLException e) {
				System.out.println("Fail retrive data");
				e.printStackTrace();
			}
		}else {
			try {
				//prepare sql statement with dynamic value
				String sql = "SELECT po.po_co, stu.stu_name, intake.sem_co, stu.in_sem_id, gra.sem_co, stu.gra_sem_id, stu.pho_num, stu.ic_num, stu.address, sta.sta_name||'('||mod.po_co||stu.mod_by||')', stu.mod_date FROM student stu INNER JOIN staff sta ON sta.sta_id = stu.mod_by INNER JOIN position mod ON sta.sta_po = mod.po_id INNER JOIN position po ON stu.stu_po = po.po_id  INNER JOIN sem_date intake ON stu.in_sem_id = intake.sem_id INNER JOIN sem_date gra ON stu.gra_sem_id = gra.sem_id WHERE stu.stu_id = ?";
				//Initialize value for column
				PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1, stu_id);
				//get query execute status
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
				po_co = rs.getNString(1);
				name = rs.getNString(2);
				intake_sem_co = rs.getNString(3);
				intake_sem_id = rs.getInt(4);
				graduate_sem_co = rs.getNString(5);
				graduate_sem_id = rs.getInt(6);
				phone = rs.getNString(7);
				ic = rs.getNString(8);
				address = rs.getNString(9);
				mod_by = rs.getNString(10);
				mod_date = rs.getTimestamp(11);
				}
			} catch (SQLException e) {
				System.out.println("Fail retrive data");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void selectStudentSubjectList(Connection connection, int sub_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu.stu_name, sub_stu.sub_stu_id, sub_stu.stu_id FROM subject_student sub_stu INNER JOIN student stu ON sub_stu.stu_id = stu.stu_id WHERE sub_stu.sub_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_id);
			//get query execute status
			nameList.clear();
			idList.clear();
			subStuId.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				nameList.add(rs.getNString(1));
				subStuId.add(rs.getInt(2));
				idList.add(rs.getInt(3));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectCourseEvaluate(Connection connection, int sub_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT course_ev.ev_id , ev_me.me_name , course_ev.ev_weightage FROM course_evaluate course_ev INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id WHERE course_ev.sub_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_id);
			//get query execute status
			evId.clear();
			meName.clear();
			evWeightage.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				evId.add(rs.getInt(1));
				meName.add(rs.getNString(2));
				evWeightage.add(rs.getFloat(3));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean checkStudentEvaluation(Connection connection, int sub_stu_id, int ev_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu_ev_id FROM student_evaluate WHERE sub_stu_id = ? AND ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_stu_id);
			ps.setInt(2, ev_id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertStudentEvaluation(Connection connection, int sub_stu_id, int ev_id, float marks, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO student_evaluate (sub_stu_id, ev_id, marks, mod_by, mod_date) VALUES (?, ?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			ps.setInt(2, ev_id);
			ps.setFloat(3, marks);
			ps.setInt(4, sta_id);
			ps.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
			//get query execute status
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}  
		return false;
	}

	@Override
	public void selectStudentEvaluationByCourseEvaluate(Connection connection, int ev_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu_ev.stu_ev_id, stu.stu_name, stu.stu_id, stu_ev.marks, sta.sta_name||'('||po.po_co||stu_ev.mod_by||')', stu_ev.mod_date FROM student_evaluate stu_ev INNER JOIN subject_student sub_stu ON stu_ev.sub_stu_id = sub_stu.sub_stu_id INNER JOIN student stu ON stu.stu_id = sub_stu.stu_id INNER JOIN staff sta ON stu_ev.mod_by = sta.sta_id INNER JOIN position po ON po.po_id = sta.sta_po WHERE (stu_ev.ev_id = ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, ev_id);
			//get query execute status
			stuEvId.clear();
			nameList.clear();
			idList.clear();
			marks.clear();
			modBy.clear();
			modDate.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				stuEvId.add(rs.getInt(1));
				nameList.add(rs.getNString(2));
				idList.add(rs.getInt(3));
				marks.add(rs.getFloat(4));
				modBy.add(rs.getNString(5));
				modDate.add(rs.getTimestamp(6));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectStudentEvaluationByStudent(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT ev_me.me_name, stu_ev.stu_ev_id,stu_ev.marks, sta.sta_name||'('||po.po_co||stu_ev.mod_by||')', stu_ev.mod_date FROM student_evaluate stu_ev INNER JOIN course_evaluate course_ev ON stu_ev.ev_id = course_ev.ev_id INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id INNER JOIN staff sta ON stu_ev.mod_by = sta.sta_id INNER JOIN position po ON po.po_id = sta.sta_po WHERE stu_ev.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			totalMark = 0;
			meName.clear();
			stuEvId.clear();
			marks.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				meName.add(rs.getString(1));
				stuEvId.add(rs.getInt(2));
				marks.add(rs.getFloat(3));
				modBy.add(rs.getNString(4));
				modDate.add(rs.getTimestamp(5));
				totalMark += rs.getFloat(3);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public void selectStudentEvaluationByStudentEvaluateId(Connection connection, int stu_ev_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT ev_me.me_name, stu_ev.ev_id, stu_ev.marks, stu.stu_id, stu.stu_name,sub_stu.sub_id FROM student_evaluate stu_ev INNER JOIN course_evaluate course_ev ON stu_ev.ev_id = course_ev.ev_id INNER JOIN evaluate_method ev_me ON ev_me.me_id = course_ev.me_id  INNER JOIN subject_student sub_stu ON stu_ev.sub_stu_id = sub_stu.sub_stu_id INNER JOIN student stu ON sub_stu.stu_id = stu.stu_id WHERE stu_ev.stu_ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_ev_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			meName.clear();
			evId.clear();
			marks.clear();
			idList.clear();
			nameList.clear();
			subId.clear();
			while(rs.next()) {
				meName.add(rs.getString(1));
				evId.add(rs.getInt(2));
				marks.add(rs.getFloat(3));
				idList.add(rs.getInt(4));
				nameList.add(rs.getString(5));
				subId.add(rs.getInt(6));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}	
	
	@Override
	public void selectStudentEvaluationBySubject(Connection connection, int sub_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT total.sub_stu_id, stu.stu_name, sub_stu.stu_id, total.total_marks FROM (SELECT sub_stu_id, SUM(marks) AS total_marks FROM student_evaluate GROUP BY sub_stu_id��total INNER JOIN subject_student sub_stu ON total.sub_stu_id = sub_stu.sub_stu_id INNER JOIN student stu ON stu.stu_id = sub_stu.stu_id INNER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id WHERE sub_li.sub_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_id);
			//get query execute status
			subStuId.clear();
			nameList.clear();
			idList.clear();
			total.clear();
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				subStuId.add(rs.getInt(1));
				nameList.add(rs.getNString(2));
				idList.add(rs.getInt(3));
				total.add(rs.getFloat(4));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}

	}

	@Override
	public boolean updateStudentEvaluation(Connection connection, int stu_ev_id, float marks, int sta_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE student_evaluate SET marks = ?, mod_by = ?, mod_date = ? WHERE stu_ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setFloat(1, marks);
			ps.setInt(2, sta_id);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(4, stu_ev_id);
			//get query execute status
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail update data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteStudentEvaluation(Connection connection, int stu_ev_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM student_evaluate WHERE stu_ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_ev_id);
			//get query execute status
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertStudentSubject(Connection connection, int sub_id, int stu_id, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO subject_student (sub_id, stu_id, mod_by, mod_date) VALUES (?, ?, ?, ?)";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, stu_id);
			ps.setInt(3, mod_by);
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void selectStudentSubject(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu.stu_name, stu.stu_id, sub_stu.sub_stu_id , sub_stu.sub_id , sub.sub_name , sub.sub_co , sub_li.sec , sem.sem_co , sub_li.sem_id , sta.sta_name||'('||po.po_co||sub_stu.mod_by||')' , sub_stu.mod_date FROM subject_student sub_stu LEFT OUTER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id LEFT OUTER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id LEFT OUTER JOIN staff sta ON sub_stu.mod_by = sta.sta_id LEFT OUTER JOIN position po ON po.po_id = sta.sta_po LEFT OUTER JOIN subject sub ON sub.sub_co = sub_li.sub_co INNER JOIN student stu ON sub_stu.stu_id = stu.stu_id INNER JOIN position stu_po ON stu.stu_po = stu_po.po_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			nameList.clear();
			idList.clear();
			subStuId.clear();
			subId.clear();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				nameList.add(rs.getNString(1));
				idList.add(rs.getInt(2));
				subStuId.add(rs.getInt(3));
				subId.add(rs.getInt(4));
				subName.add(rs.getNString(5));
				subCo.add(rs.getInt(6));
				sec.add(rs.getInt(7));
				semCo.add(rs.getNString(8));
				semId.add(rs.getInt(9));
				modBy.add(rs.getNString(10));
				modDate.add(rs.getTimestamp(11));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectStudentSubject(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu.stu_name, stu.stu_id, sub_stu.sub_stu_id , sub_stu.sub_id , sub.sub_name , sub.sub_co , sub_li.sec , sem.sem_co , sub_li.sem_id , sta.sta_name||'('||po.po_co||sub_stu.mod_by||')' , sub_stu.mod_date FROM subject_student sub_stu LEFT OUTER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id LEFT OUTER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id LEFT OUTER JOIN staff sta ON sub_stu.mod_by = sta.sta_id LEFT OUTER JOIN position po ON po.po_id = sta.sta_po LEFT OUTER JOIN subject sub ON sub.sub_co = sub_li.sub_co INNER JOIN student stu ON sub_stu.stu_id = stu.stu_id INNER JOIN position stu_po ON stu.stu_po = stu_po.po_id WHERE sub_stu.stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			nameList.clear();
			idList.clear();
			subStuId.clear();
			subId.clear();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();
			semId.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				nameList.add(rs.getNString(1));
				idList.add(rs.getInt(2));
				subStuId.add(rs.getInt(3));
				subId.add(rs.getInt(4));
				subName.add(rs.getNString(5));
				subCo.add(rs.getInt(6));
				sec.add(rs.getInt(7));
				semCo.add(rs.getNString(8));
				semId.add(rs.getInt(9));
				modBy.add(rs.getNString(10));
				modDate.add(rs.getTimestamp(11));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateStudentSubject(Connection connection, int sub_id, int mod_by, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE subject_student SET sub_id = ?, mod_by = ?, mod_date = ? WHERE sub_stu_id= ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_id);
			ps.setInt(2, mod_by);
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(4, sub_stu_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail update data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteStudentSubject(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM subject_student WHERE sub_stu_id= ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, sub_stu_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void selectStudentSubjectBySubStuId(Connection connection, int sub_stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu.stu_name, stu.stu_id, stu_po.po_co,sub_stu.sub_id , sub.sub_name , sub.sub_co , sub_li.sec , sem.sem_co FROM subject_student sub_stu INNER JOIN subject_list sub_li ON sub_li.sub_id = sub_stu.sub_id INNER JOIN sem_date sem ON sub_li.sem_id = sem.sem_id INNER JOIN subject sub ON sub.sub_co = sub_li.sub_co INNER JOIN student stu ON sub_stu.stu_id = stu.stu_id INNER JOIN position stu_po ON stu.stu_po = stu_po.po_id WHERE sub_stu.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			/*idList.clear();
			nameList.clear();
			poCo.clear();
			subId.clear();
			subName.clear();
			subCo.clear();
			sec.clear();
			semCo.clear();*/
			while(rs.next()) {
				name = rs.getNString(1);
				id = rs.getInt(2);
				po_co = rs.getNString(3);
				sub_id = rs.getInt(4);
				sub_name = rs.getNString(5);
				sub_co = rs.getInt(6);
				section = rs.getInt(7);
				sem_co = rs.getNString(8);
				
				nameList.add(rs.getNString(1));
				idList.add(rs.getInt(2));
				poCo.add(rs.getNString(3));
				subId.add(rs.getInt(4));
				subName.add(rs.getNString(5));
				subCo.add(rs.getInt(6));
				sec.add(rs.getInt(7));
				semCo.add(rs.getNString(8));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectAllStudentInfo(Connection connection) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT po.po_co, stu.stu_id, stu.stu_name, intake.sem_co, stu.in_sem_id, gra.sem_co, stu.gra_sem_id, stu.pho_num, stu.ic_num, stu.address, sta.sta_name||'('||mod.po_co||stu.mod_by||')', stu.mod_date FROM student stu LEFT OUTER JOIN staff sta ON sta.sta_id = stu.mod_by LEFT OUTER JOIN position po ON stu.stu_po = po.po_id LEFT OUTER JOIN position mod ON sta.sta_po = mod.po_id LEFT OUTER JOIN sem_date intake ON stu.in_sem_id = intake.sem_id LEFT OUTER JOIN sem_date gra ON stu.gra_sem_id = gra.sem_id ORDER BY stu.stu_id";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			poCo.clear();
			idList.clear();
			nameList.clear();
			inSemCo.clear();
			inSemId.clear();
			graSemCo.clear();
			graSemId.clear();
			phoneNumber.clear();
			icNumber.clear();
			addressInfo.clear();
			modBy.clear();
			modDate.clear();
			while(rs.next()) {
				poCo.add(rs.getNString(1));
				idList.add(rs.getInt(2));
				nameList.add(rs.getNString(3));
				inSemCo.add(rs.getNString(4));
				inSemId.add(rs.getInt(5));
				graSemCo.add(rs.getNString(6));
				graSemId.add(rs.getInt(7));
				phoneNumber.add(rs.getNString(8));
				icNumber.add(rs.getNString(9));
				addressInfo.add(rs.getNString(10));
				modBy.add(rs.getNString(11));
				modDate.add(rs.getTimestamp(12));
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateStudentInfo(Connection connection, String stu_name, int in_sem_id, int gra_sem_id, String pho_num, String ic_num, String address, int mod_by, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "UPDATE student SET stu_name = ?, in_sem_id = ?, gra_sem_id = ?, pho_num = ?, ic_num = ?, address = ?, mod_by = ?, mod_date = ? WHERE stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, stu_name);
			ps.setInt(2, in_sem_id);
			ps.setInt(3, gra_sem_id);
			ps.setNString(4, pho_num);
			ps.setNString(5, ic_num);
			ps.setNString(6, address);
			ps.setInt(7, mod_by);
			ps.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setInt(9, stu_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkStudentInfo(Connection connection, String ic_num) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT stu_id FROM student WHERE ic_num = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, ic_num);
			rs = ps.executeQuery();
			if (rs.next()) {
			    return false;
			} else {
			    return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean insertStudentInfo(Connection connection, String stu_name, int in_sem_id, int gra_sem_id, String pho_num, String ic_num, String address, int mod_by) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "INSERT INTO student (stu_name, stu_password, in_sem_id, gra_sem_id, stu_po, pho_num, ic_num, address, mod_by, mod_date)  VALUES (?, ?, ?, ?, 3, ?, ?, ?, ?, ?)" ;
			//Initialize value for column
			PreparedStatement  ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setNString(1, stu_name);
			ps.setNString(2, AES.encrypt(ic_num));
			ps.setInt(3, in_sem_id);
			ps.setInt(4, gra_sem_id);
			ps.setNString(5, pho_num);
			ps.setNString(6, ic_num);
			ps.setNString(7, address);
			ps.setInt(8, mod_by);
			ps.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail insert data");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteStudentInfo(Connection connection, int stu_id) {
		// TODO Auto-generated method stub
		
		try {
			//prepare sql statement with dynamic value
			String sql = "DELETE FROM student WHERE stu_id= ?";
			//Initialize value for column
			PreparedStatement  ps = connection.prepareStatement(sql);
			//get query execute status
			ps.setInt(1, stu_id);
			int status = ps.executeUpdate();
			if (status == 0) {
				return false;
			}else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Fail delete data");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public String selectSemCode(Connection connection, int sem_id) {
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sem_co FROM sem_date WHERE sem_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sem_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getNString(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public String selectSubjectName(Connection connection, int sub_stu_id) {
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub.sub_name FROM subject sub INNER JOIN subject_list sub_li ON sub.sub_co = sub_li.sub_co INNER JOIN subject_student sub_stu ON sub_li.sub_id = sub_stu.sub_id WHERE sub_stu.sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getNString(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public int selectSubjectIdByStudent(Connection connection, int sub_stu_id) {
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_id FROM subject_student WHERE sub_stu_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, sub_stu_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public int selectSubjectIdByCourseEvaluate(Connection connection, int ev_id) {
		try {
			//prepare sql statement with dynamic value
			String sql = "SELECT sub_id FROM course_evaluate WHERE ev_id = ?";
			//Initialize value for column
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, ev_id);
			//get query execute status
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("Fail retrive data");
			e.printStackTrace();
		}
		return 0;
	}
	
	
}
