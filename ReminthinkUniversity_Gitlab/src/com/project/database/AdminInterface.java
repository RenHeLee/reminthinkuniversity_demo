package com.project.database;
import java.sql.Connection;
import java.util.Date;

public interface AdminInterface {
	void selectAllStaffInfo(Connection connection);
	boolean updateStaffInfo(Connection connection, String sta_name, int sta_po, Date fir_ser_date, Date las_ser_date, String pho_num, String ic_num, String address, int mod_by, int sta_id);
	boolean checkStaffInfo(Connection connection, String ic_num);
	boolean insertStaffInfo(Connection connection, String sta_name, int sta_po, Date fir_ser_date, Date las_ser_date, String pho_num, String ic_num, String address, int mod_by);
	boolean deleteStaffInfo(Connection connection, int sta_id);
	
	void selectSemInWorkload(Connection connection);
	void selectAllWorkload(Connection connection);
	void selectWorkload(Connection connection, int wo_id);
	boolean updateWorkload(Connection connection, int sub_id, int sta_id, int mod_by, int wl_id);
	boolean checkWorkload(Connection connection, int sub_id, int sta_id);
	boolean insertWorkload(Connection connection, int sub_id, int sta_id, int mod_by);
	boolean deleteWorkload(Connection connection, int wl_id);
	
	void selectAllCourseEvaluate(Connection connection);
	void selectCourseEvaluate(Connection connection);
	void selectCourseEvaluate(int ev_id, Connection connection);
	boolean updateCourseEvaluate(Connection connection, int sub_id, int me_id, float ev_weightage, int mod_by, int ev_id);
	boolean checkCourseEvaluate(Connection connection, int sub_id, int me_id, float ev_weightage);
	boolean insertCourseEvaluate(Connection connection, int sub_id, int me_id, float ev_weightage, int mod_by);
	boolean deleteCourseEvaluate(Connection connection, int ev_id);
	
	void selectAllSubject(Connection connection);
	void selectSubject(Connection connection, int sub_co);
	boolean updateSubject(Connection connection, String sub_name, int mod_by, int sub_co);
	boolean checkSubject(Connection connection, String sub_name);
	boolean insertSubject(Connection connection, String sub_name, int mod_by);
	boolean deleteSubject(Connection connection, int sub_co);
	
	void selectAllSubjectBySection(Connection connection);
	void selectSubjectBySection(Connection connection, int sub_id);
	boolean updateSubjectBySection(Connection connection, int sub_co, int sec, int sem_id, int mod_by, int sub_id);
	boolean checkSubjectBySection(Connection connection, int sub_co, int sec, int sem_id);
	boolean insertSubjectBySection(Connection connection, int sub_co, int sec, int sem_id, int mod_by);
	boolean deleteSubjectBySection(Connection connection, int sub_id);
	
	void selectAllSemDate(Connection connection);
	void selectSemDate(Connection connection, int sem_id);
	boolean updateSemDate(Connection connection, Date in_date, Date gra_date, String sem_co, int mod_by, int sem_id);
	boolean checkSemDate(Connection connection, String sem_co);
	boolean insertSemDate(Connection connection, Date in_date, Date gra_date, String sem_co, int mod_by);
	boolean deleteSemDate(Connection connection, int sem_id);
	
	void selectAllEvaluateMethod(Connection connection);
	void selectAllEvaluateMethod(Connection connection, int me_id);
	boolean updateEvaluateMethod(Connection connection, String me_name, int mod_by, int me_id);
	boolean checkEvaluateMethod(Connection connection, String me_name);
	boolean insertEvaluateMethod(Connection connection, String me_name, int mod_by);
	boolean deleteEvaluateMethod(Connection connection, int me_id);
	
	public boolean updateStudentSubject(Connection connection, int sub_id, int mod_by, int sub_stu_id);
	public boolean insertStudentSubject(Connection connection, int sub_id, int stu_id, int mod_by);
	
	public void selectAllPosition(Connection connection);
	public boolean updatePosition(Connection connection, String po_co, String po_title, int mod_by, int po_id);
	boolean checkPosition(Connection connection, String po_co, String po_title);
	public boolean insertPosition(Connection connection, String po_co, String po_title, int mod_by);
	public boolean deletePosition(Connection connection, int po_id);
	
}
