package com.project.database;

import java.sql.Connection;

public interface LecturerInterface {
	void selectStaffInfo(Connection connection, int sta_id);
	boolean updateStaffInfo(Connection connection, int sta_id, String pho_num, String address);
	
	void selectWorkload(Connection connection, int sta_id);
	void selectSemInWorkload(Connection connection, int sta_id);
	void selectWorkload(Connection connection, int sta_id, int sem_id);

	boolean checkStudentSubject(Connection connection, int sub_id, int stu_id);
	void selectStudentSubject(Connection connection, int stu_id);
	void selectStudentSubject(Connection connection);
	void selectStudentSubjectBySubStuId(Connection connection, int sub_stu_id); 
	boolean deleteStudentSubject(Connection connection, int sub_stu_id);
	public boolean updateStudentSubject(Connection connection, int sub_id, int mod_by, int sub_stu_id);
	public boolean insertStudentSubject(Connection connection, int sub_id, int stu_id, int mod_by);
	
	void selectStudentInfo(Connection connection, int stu_id);
	void selectStudentSubjectList(Connection connection, int sub_id);
	
	void selectCourseEvaluate(Connection connection, int sub_id);
	
	boolean checkStudentEvaluation(Connection connection, int sub_stu_id, int ev_id);
	boolean insertStudentEvaluation(Connection connection, int sub_stu_id, int ev_id, float marks, int sta_id);
	void selectStudentEvaluationBySubject(Connection connection, int sub_id);
	void selectStudentEvaluationByCourseEvaluate(Connection connection, int ev_id);
	void selectStudentEvaluationByStudent(Connection connection, int sub_stu_id);
	boolean updateStudentEvaluation(Connection connection, int stu_ev_id, float marks, int sta_id);
	boolean deleteStudentEvaluation(Connection connection, int stu_ev_id);
	void selectStudentEvaluationByStudentEvaluateId(Connection connection, int stu_ev_id);
	
	void selectAllStudentInfo(Connection connection);
	boolean updateStudentInfo(Connection connection, String stu_name, int in_sem_id, int gra_sem_id, String pho_num, String ic_num, String address, int mod_by, int stu_id);
	boolean checkStudentInfo(Connection connection, String ic_num);
	boolean insertStudentInfo(Connection connection, String stu_name, int in_sem_id, int gra_sem_id, String pho_num, String ic_num, String address, int mod_by);
	boolean deleteStudentInfo(Connection connection, int stu_id);
	
	String selectSemCode(Connection connection, int sem_id);
	String selectSubjectName(Connection connection, int sub_stu_id);
	int selectSubjectIdByStudent(Connection connection, int sub_stu_id);
	int selectSubjectIdByCourseEvaluate(Connection connection, int ev_id);
}
