package com.project.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseConnection {

	static Connection connection = null;
	static String serverName = "localhost";
	static String portNumber = "1521";
	static String sid = "xe";

	static String username = "system";
	static String password = "1234";
	static String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		databaseConnecton();
	}

	public static void databaseConnecton() {
		System.out.println("-------- Oracle JDBC Connection Testing ------");
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return;
		}

		System.out.println("Oracle JDBC Driver Registered!");

		try {
			// Create a connection to the database
			connection = DriverManager.getConnection(url, username, password);
		}catch (SQLException e) {
			//Could not connect to the database
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		System.out.println("JDBC connected");
	}

}
