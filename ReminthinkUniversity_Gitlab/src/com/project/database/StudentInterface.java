package com.project.database;

import java.sql.Connection;

public interface StudentInterface {
	
	void selectStudentInfo(Connection connection, int stu_id);
	boolean updateStudentInfo(Connection connection, int stu_id, String pho_num, String address);
	void selectAllSubjectBySection(Connection connection);
	boolean checkStudentSubject(Connection connection, int sub_id, int stu_id);
	boolean insertStudentSubject(Connection connection, int sub_id, int stu_id);
	void selectStudentSubject(Connection connection, int stu_id);
	boolean updateStudentSubject(Connection connection, int sub_id, int sub_stu_id);
	void selectStudentSubjectBySubStuId(Connection connection, int sub_stu_id); 
	boolean deleteStudentSubject(Connection connection, int sub_stu_id);
	void selectSemInStudentEvaluation(Connection connection);
	void selectSemInStudentEvaluation(Connection connection, int stu_id);
	String selectSemCode(Connection connection, int sem_id);
	String selectSubjectName(Connection connection, int sub_stu_id);
	void selectStudentEvaluationWholeStudy(Connection connection, int stu_id);
	void selectStudentEvaluationBySem(Connection connection, int stu_id,  int sem_id);
	void selectStudentEvaluationByCourse(Connection connection, int sub_stu_id);
}
