package com.project.database;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;


public class AesTesting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String originalString = "Student1234";

		String encryptedString = AES.encrypt(originalString) ;
		String decryptedString = AES.decrypt(encryptedString) ;

		System.out.println(originalString);
		System.out.println(encryptedString);
		System.out.println(decryptedString);

		Student student = new Student();
		Connection connection = student.databaseConnection();
		int sem_id = 1;
		System.out.println(student.selectSemCode(connection, sem_id));
	}
}