<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();

	int sub_co = Integer.parseInt(request.getParameter("sub_co"));
	String sub_name = request.getParameter("sub_name");

	if(sub_name != ""){
		staff.updateSubject(connection, sub_name, sta_id, sub_co);
	}
	
	staff.selectSubject(connection, sub_co);
%>
<h4>Update subject</h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Subject:</td>
				<td><input type="text" id="sub_name" value="<%out.print(staff.getSubName().get(0)); %>"></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="updateSubject(<%=sub_co%>)">Update</button>
	<button type="button" class="btn btn-warning" onclick="displayInsertSubject()">Discard</button>
</form>
