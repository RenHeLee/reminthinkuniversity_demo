<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<p>
<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

int id = Integer.parseInt(request.getParameter("id"));

if(staff.deleteStaffInfo(connection, id)){
	out.print("This staff's record is deleted successfully.");
}else{
	out.print("This staff's record cannot be deleted.");
}

%>
</p>
<button type='button' class='btn btn-info' onclick='displayInsertStaffInformation()'>Back</button>