<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
Student student = new Student();
Connection connection = student.databaseConnection();

ArrayList<Integer> subStuId = new ArrayList<Integer>();
ArrayList<Integer> subId = new ArrayList<Integer>();
ArrayList<String> subName = new ArrayList<String>();
ArrayList<Integer> subCo = new ArrayList<Integer>();
ArrayList<Integer> sec = new ArrayList<Integer>();
ArrayList<String> semCo = new ArrayList<String>();
ArrayList<Integer> semId = new ArrayList<Integer>();
ArrayList<String> modBy = new ArrayList<String>();
ArrayList<Date> modDate  = new ArrayList<Date>();
ArrayList<String> meName = new ArrayList<String>();
ArrayList<Float> marks = new ArrayList<Float>();
ArrayList<Float> total = new ArrayList<Float>();

student.selectStudentSubject(connection, stu_id);
subStuId = student.getSubStuId();
subId = student.getSubId();
subName = student.getSubName();
subCo = student.getSubCo();
sec = student.getSec();
semCo = student.getSemCo();
semId = student.getSemId();
modBy = student.getModBy();
modDate = student.getModDate();

student.selectStudentInfo(connection, stu_id);
String studentModBy = student.getName() + "(" + student.getPo_co() + stu_id + ")";

%>
<h4>Select subject</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No</td>
			<td>Subject</td>
			<td>Section</td>
			<td>Semester</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		<%
		for (int i = 0; i < subStuId.size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + subName.get(i) + "</td>");
			out.println("<td>" + sec.get(i) + "</td>");
			out.println("<td>" + semCo.get(i) + "</td>");
			if(modBy.get(i).equalsIgnoreCase("()")){
				out.println("<td>" + studentModBy + "</td>");
			}else{
				out.println("<td>" + modBy.get(i) + "</td>");
			}
			out.println("<td>" + modDate.get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayFormUpdateStudentSubject(" + subStuId.get(i) + ")'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteStudentSubject(" + subStuId.get(i) + ")'>Delete</button></td></tr>");
		}
		%>

	</tbody>
</table>