<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();
int sem_id = 0;
sem_id = Integer.parseInt(request.getParameter("sem_id"));
String sem_co = request.getParameter("sem_co");
String firstDate = request.getParameter("in_date");
String lastDate = request.getParameter("gra_date");
Date in_date = null;
Date gra_date = null;

if(firstDate != ""){
	in_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("in_date"));
}
if(lastDate != ""){
	gra_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("gra_date"));
}

staff.selectSemDate(connection, sem_id);

if (sem_co != "" && firstDate != "" && lastDate != ""){
	staff.updateSemDate(connection, in_date, gra_date, sem_co, sta_id, sem_id);
}
%>
<h4>Update semester information</h4>
<form>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>Semester: </td>
			<td><input type="text" id="sem_co" value="<%out.print(staff.getSemCo().get(0));%>"></td>
		</tr>
		<tr>
			<td>Starting from: </td>
			<td><input type="date" id="in_date" value="<%out.print(staff.getFirDate().get(0));%>"></td>
		</tr>
		<tr>
			<td>Until to: </td>
			<td><input type="date" id="gra_date" value="<%out.print(staff.getLasDate().get(0)); %>"></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-success" onclick="updateSemDate(<%=sem_id %>)">Update</button>
<button type="button" class="btn btn-danger" onclick="displayInsertSemDate()">Discard</button>
</form>
