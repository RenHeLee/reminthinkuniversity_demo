<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.User"%>

<%
	String inputId = request.getParameter("id");
	String password = request.getParameter("password");
	String poCo = inputId.substring(0, 1);
	int privilege = -1;

	if (poCo.equalsIgnoreCase("a") || poCo.equalsIgnoreCase("l")) {
		privilege = 0;
	} else if (poCo.equalsIgnoreCase("s")) {
		privilege = 1;
	}

	String userId = inputId.substring(1);
	int id = 0;
	try {
		id = Integer.parseInt(userId);
	} catch (NumberFormatException ex) {
		response.sendRedirect("index.jsp?message=Invalid%20login%21Please%20try%20again%2e");
	}
	User user = new User();
	Date nowDate = new Date();

	if (user.login(privilege, id, password)) {
		Date validDate = user.getValidLoginDate();
		String name = user.getUsername();
		int poId = user.getPosition();
		if (validDate == null) {
			response.sendRedirect("index.jsp?message=Invalid%20login%21Please%20try%20again%2e");
		} else if (validDate.compareTo(nowDate) >= 0) {
			session.setAttribute("ReminthinkUniversityId", id);
			session.setAttribute("ReminthinkUniversityPo", poId);

			switch (poId) {
			case 1:
				session.setMaxInactiveInterval(28800);
				response.sendRedirect("admin.jsp");
				break;
			case 2:
				session.setMaxInactiveInterval(3600);
				response.sendRedirect("lecturer.jsp");
				break;
			case 3:
				session.setMaxInactiveInterval(900);
				response.sendRedirect("student.jsp");
				break;
			}
		} else {
			response.sendRedirect(
					"index.jsp?message=Your%20account%20expired%21Please%20contact%20with%20admin%2e");
		}
	} else {
		response.sendRedirect("index.jsp?message=Invalid%20login%21Please%20try%20again%2e");
	}
%>