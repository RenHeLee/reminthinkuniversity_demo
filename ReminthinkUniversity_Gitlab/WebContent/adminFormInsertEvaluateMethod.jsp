<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();
	
	String me_name = request.getParameter("me_name");
	String successMessage = "";
	String errorMessage = "";
	if(me_name != ""){
		if(staff.checkEvaluateMethod(connection, me_name)){
			staff.insertEvaluateMethod(connection, me_name, sta_id);
			successMessage = "This evaluate method is inserted successfully.";
		}else{
			errorMessage = "Please ensure evaluate method is unique.";
		}
	}
	
%>
<h4>Insert Evaluate Method</h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Evaluate method:</td>
				<td><input type='text' id='me_name'></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="insertEvaluateMethod()">Insert</button>
	<input type="reset" value="Reset" class="btn btn-warning" >
</form>
