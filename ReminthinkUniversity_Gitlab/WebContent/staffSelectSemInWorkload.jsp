<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

if (po_id == 1){
	staff.selectSemInWorkload(connection);
} else if (po_id == 2){
	staff.selectSemInWorkload(connection, sta_id);
}

%>

<form>
	Semester:
	<select id='semId'>
	<%
				for (int i = 0; i < staff.getSemId().size(); i++){
					out.println("<option value="+ staff.getSemId().get(i) +">" + staff.getSemCo().get(i) + "</option>");
				}
				%>
	</select>
	<button type="button" class="btn btn-success"
		onclick="displaySelectionWorkloadBySemFromDropDownList()">Select</button>
		<button type="button" class="btn btn-info"
		onclick="displayLecturerWorkload()">Whole Works</button>
</form>