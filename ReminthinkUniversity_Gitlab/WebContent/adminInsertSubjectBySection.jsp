<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

int sub_co = Integer.parseInt(request.getParameter("sub_co"));
int sec = Integer.parseInt(request.getParameter("sec"));
int sem_id = Integer.parseInt(request.getParameter("sem_id"));

if(staff.checkSubjectBySection(connection, sub_co, sec, sem_id)){
	out.print(staff.insertSubjectBySection(connection, sub_co, sec, sem_id, sta_id));
}else{
	out.print("Repeated");
}

%>