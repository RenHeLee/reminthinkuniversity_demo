<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>


<%
Student student = new Student();
Connection connection = student.databaseConnection();

int sub_stu_id = Integer.parseInt(request.getParameter("sub_stu_id"));
String sub_name = student.selectSubjectName(connection, sub_stu_id);
student.selectStudentEvaluationByCourse(connection, sub_stu_id);
%>

<h3><%=sub_name %></h3>
<table class="table table-hover">
	<tbody>
		<tr>
			<td style="width: 5%;">No.</td>
			<td style="width: 15%;">Evaluate method</td>
			<td style="width: 15%;">Marks</td>
			<td style="width: 15%;">Modified by</td>
			<td style="width: 20%;">Modified date</td>
		</tr>
		<%
		for (int i = 0; i < student.getMeName().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + student.getMeName().get(i) + "</td>");
			out.println("<td>" + student.getMarks().get(i) + "</td>");
			out.println("<td>" + student.getModBy().get(i)  + "</td>");
			out.println("<td>" + student.getModDate().get(i) + "</td></tr>");
		}
		%>
	</tbody>
</table>
<h4>Total marks: <%=student.getTotalMark() %></h4>
