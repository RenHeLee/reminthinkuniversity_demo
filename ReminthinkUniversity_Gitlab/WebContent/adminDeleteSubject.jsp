<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<p>
<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

int sub_co = Integer.parseInt(request.getParameter("sub_co"));

if(staff.deleteSubject(connection, sub_co)){
	out.print("This student's record is deleted successfully.");
}else{
	out.print("This student's record cannot be deleted.");
}

%>
</p><button type='button' class='btn btn-info' onclick='displayInsertSubject()'>Back</button>