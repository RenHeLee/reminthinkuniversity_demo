<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Lecturer staff = new Lecturer();
Connection connection = staff.databaseConnection();

staff.selectAllStudentInfo(connection);

%>

<h4>All Student Information</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>ID</td>
			<td>Name</td>
			<td>Phone</td>
			<td>IC</td>
			<td>Address</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Details</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		
		<%
		for (int i = 0; i < staff.getIdList().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getPoCo().get(i) + staff.getIdList().get(i) + "</td>");
			out.println("<td>" + staff.getNameList().get(i) + "</a></td>");
			out.println("<td>" + staff.getPhoneNumber().get(i) + "</td>");
			out.println("<td>" + staff.getIcNumber().get(i) + "</td>");
			out.println("<td>" + staff.getAddressInfo().get(i) + "</td>");
			if(staff.getModBy().get(i).equalsIgnoreCase("()")){
				out.println("<td>" + staff.getNameList().get(i) + "(" + staff.getPoCo().get(i) + staff.getIdList().get(i) + ")" + "</td>");
			}else{
				out.println("<td>" + staff.getModBy().get(i) + "</td>");
			}
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-info' onclick='displayStudentInformationByStudentId(" + staff.getIdList().get(i) + ")'>Details</button></td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayUpdateStudentForm(" + staff.getIdList().get(i) + ")'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteStudentInformationByStudentId(" + staff.getIdList().get(i) + ")'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>