<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();
	String successMessage = "";
	String errorMessage = "";
	
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	int me_id = Integer.parseInt(request.getParameter("me_id"));

	float ev_weightage = Float.parseFloat(request.getParameter("ev_weightage"));

	if(sub_id == 0 && me_id == 0 && ev_weightage == 0){
	}else if (sub_id != 0 && me_id != 0 && ev_weightage != 0){
		if(staff.checkCourseEvaluate(connection, sub_id, me_id, ev_weightage)){
			staff.insertCourseEvaluate(connection, sub_id, me_id, ev_weightage, sta_id);
			successMessage = "This semester record is inserted successfully.";
		}else{
			errorMessage = "Please ensure semester code is unique.";
		}
	}else{
		errorMessage = "Please ensure all column are not empty.";
	}
	
%>
<h4>Insert course evaluate</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Subject:</td>
				<td><select id="sub_id">
						<%
							staff.selectAllSubjectBySection(connection);
							for (int i = 0; i < staff.getSubCo().size(); i++) {
								out.println(
										"<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + "(Section " + staff.getSec().get(i) + ")" + "</option>");
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Method</td>
				<td><select id="me_id">
						<%
						staff.selectAllEvaluateMethod(connection);
						for (int i = 0; i < staff.getMeId().size(); i++) {
							out.println(
									"<option value=" + staff.getMeId().get(i) + ">" + staff.getMeName().get(i) + "</option>");
						}
					
						%>
				</select></td>
			</tr>
			<tr>
				<td>Weightage</td>
				<td><input type="number" required id="ev_weightage" min="0" value="" step=".01"></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="insertCourseEvaluate()">Insert</button>
	<input type="reset" value="Reset" class="btn btn-warning" >
</form>
