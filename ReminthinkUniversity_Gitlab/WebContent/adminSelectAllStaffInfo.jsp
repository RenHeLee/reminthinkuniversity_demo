<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

staff.selectAllStaffInfo(connection);

%>

<h4>Staff information</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>ID</td>
			<td>Name</td>
			<td>Position</td>
			<td>First service date</td>
			<td>Last service date</td>
			<td>Phone</td>
			<td>IC</td>
			<td>Address</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		
		<%
		for (int i = 0; i < staff.getIdList().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getPoCo().get(i) + staff.getIdList().get(i) + "</td>");
			out.println("<td>" + staff.getNameList().get(i) + "</a></td>");
			out.println("<td>" + staff.getPoTitle().get(i) + "</td>");
			out.println("<td>" + staff.getFirDate().get(i) + "</td>");
			out.println("<td>" + staff.getLasDate().get(i) + "</td>");
			out.println("<td>" + staff.getPhoneNumber().get(i) + "</td>");
			out.println("<td>" + staff.getIcNumber().get(i) + "</td>");
			out.println("<td>" + staff.getAddressInfo().get(i) + "</td>");
			if(staff.getModBy().get(i).equalsIgnoreCase("()")){
				out.println("<td>" + staff.getNameList().get(i) + "(" + staff.getPoCo().get(i) + staff.getIdList().get(i) + ")" + "</td>");
			}else{
				out.println("<td>" + staff.getModBy().get(i) + "</td>");
			}
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayUpdateStaffInformation(" + staff.getIdList().get(i) + ")'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteStaffInformation(" + staff.getIdList().get(i) + ")'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>