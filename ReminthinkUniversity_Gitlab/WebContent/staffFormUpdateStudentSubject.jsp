<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
	Admin staff = new Admin();
	Lecturer lecturer = new Lecturer();
	Connection connection = staff.databaseConnection();
	
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	int sub_stu_id = Integer.parseInt(request.getParameter("sub_stu_id"));
	
	staff.selectStudentSubjectBySubStuId(connection, sub_stu_id);
	
	if (sub_id != 0 && sub_stu_id != 0){
		staff.updateStudentSubject(connection, sub_id, sta_id, sub_stu_id);
	}

%>
<h4>Update Student Subject</h4>
<form>
	Student name: <select disabled>
		<%
			lecturer.selectStudentSubjectBySubStuId(connection, sub_stu_id);
			out.println("<option value=" + lecturer.getIdList().get(0) + ">" + lecturer.getNameList().get(0) + " (S"
					+ lecturer.getIdList().get(0) + ")" + "</option>");
		%>
	</select> Subject and section: <select id="sub_id">
		<%
			out.println("<option value=" + lecturer.getSubId().get(0) + ">" + lecturer.getSubName().get(0) + " - Section "
					+ lecturer.getSec().get(0) + "(" + lecturer.getSemCo().get(0) + ")" + "</option>");

			staff.selectAllSubjectBySection(connection);
			for (int i = 0; i < staff.getSubId().size(); i++) {
				out.println("<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + " - Section "
						+ staff.getSec().get(i) + "(" + staff.getSemCo().get(i) + ")" + "</option>");
			}
		%>
	</select>
	<button type="button" class="btn btn-success"
		onclick="updateSubjectStudent(<%=sub_stu_id%>)">Update</button>
	<button type="button" class="btn btn-danger"
		onclick="displayInsertSubjectStudentForm()">Discard</button>
</form>
