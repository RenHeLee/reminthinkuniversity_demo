<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
	Lecturer staff = new Lecturer();
	Connection connection = staff.databaseConnection();
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	staff.selectStudentSubjectList(connection, sub_id);
%>
<h4>List of Student in Xxxx for Yyyy</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>Name</td>
			<td>Matric number</td>
			<td>Details</td>
		</tr>
		
		<%
		for (int i = 0; i < staff.getIdList().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getNameList().get(i) + "</td>");
			out.println("<td>S" + staff.getIdList().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-info' onclick='displayStudentEvaluationByStudent(" + staff.getSubStuId().get(i) + ")'>Details</button></td>");
		}
		%>
	</tbody>
</table>
<button type="button" class="btn btn-primary" onclick="displayArea(2);">Back to Course Record and Evaluation</button>