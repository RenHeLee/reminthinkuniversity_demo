<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();

	int me_id = Integer.parseInt(request.getParameter("me_id"));
	String me_name = request.getParameter("me_name");
	
	if(me_name != ""){
		staff.updateEvaluateMethod(connection, me_name, sta_id, me_id);
	}
	
	staff.selectAllEvaluateMethod(connection, me_id);

%>
<h4>Update Evaluate Method</h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Evaluate method:</td>
				<%
					for(int i = 0; i < staff.getMeName().size(); i++){
						out.println("<td><input type='text' id='me_name' value='" + staff.getMeName().get(i) + "'></td>");
					}
				%>
				
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="updateEvaluateMethod(<%=me_id%>)">Update</button>
	<button type="button" class="btn btn-danger" onclick="discardEvaluateMethod()">Discard</button>
</form>
