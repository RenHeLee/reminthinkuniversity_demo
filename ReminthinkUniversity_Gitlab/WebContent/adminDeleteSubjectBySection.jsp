<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<p>
<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

int sub_id = Integer.parseInt(request.getParameter("sub_id"));

if(staff.deleteSubjectBySection(connection, sub_id)){
	out.print("This record is deleted successfully.");
}else{
	out.print("This record cannot be deleted.");
}
%>
</p><button type='button' class='btn btn-info' onclick='displayInsertSubjectBySection()'>Back</button>