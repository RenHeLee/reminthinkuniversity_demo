<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();

	int wl_id = Integer.parseInt(request.getParameter("wl_id"));
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	int id = Integer.parseInt(request.getParameter("id"));
	
	if (wl_id != 0 && sub_id != 0 && id != 0){
		staff.updateWorkload(connection, sub_id, id, sta_id, wl_id);
	}
	
%>
<h4>Update workload</h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Lecturer:</td>
				<td><select id="id" form="lecturer">
						<%
							staff.selectWorkload(connection, sta_id);
							for (int i = 0; i < staff.getIdList().size(); i++) {
								out.println(
										"<option value='" + staff.getIdList().get(i) + "'>" + staff.getNameList().get(i) + "</option>");
							}
							staff.selectAllStaffInfo(connection);
							for (int i = 0; i < staff.getIdList().size(); i++) {
								out.println(
										"<option value='" + staff.getIdList().get(i) + "'>" + staff.getNameList().get(i) + "</option>");
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Subject and section:</td>
				<td><select id="sub_id" form="subject">
						<%
							staff.selectWorkload(connection, sta_id);
							for (int i = 0; i < staff.getIdList().size(); i++) {
								out.println("<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + " - Section "
										+ staff.getSec().get(i) + "(" + staff.getSemCo().get(i) + ")" + "</option>");
							}
							staff.selectAllSubjectBySection(connection);
							for (int i = 0; i < staff.getSubId().size(); i++) {
								out.println("<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + " - Section "
										+ staff.getSec().get(i) + "(" + staff.getSemCo().get(i) + ")" + "</option>");
							}
						%>
				</select></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="updateWorkload(<%=wl_id%>)">Update</button>
	<button type="button" class="btn btn-danger" onclick="discardStaffInfo()">Discard</button>
</form>
