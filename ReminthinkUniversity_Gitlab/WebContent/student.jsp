<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ include file="checkSessionStudent.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<title>Student</title>
</head>
<body>

<script type="text/javascript">


	function displayArea(type) {
		var wholeArea = document.getElementById("wholeArea");
		var variableUpperArea = document.getElementById("variableUpperArea");
		var variableLowerArea = document.getElementById("variableLowerArea");
		var fixedUpperArea = document.getElementById("fixedUpperArea");
		var fixedLowerArea = document.getElementById("fixedLowerArea");
		if (type == 1) {
			variableUpperArea.innerHTML = "";
			variableLowerArea.innerHTML = "";
			fixedUpperArea.innerHTML = "";
			fixedLowerArea.innerHTML = "";
			wholeArea.style.display = "block";
			variableUpperArea.style.display = "none";
			variableLowerArea.style.display = "none";
			fixedUpperArea.style.display = "none";
			fixedLowerArea.style.display = "none";
		} else if (type == 2) {
			wholeArea.innerHTML = "";
			fixedUpperArea.innerHTML = "";
			fixedLowerArea.innerHTML = "";
			wholeArea.style.display = "none";
			variableUpperArea.style.display = "block";
			variableLowerArea.style.display = "block";
			fixedUpperArea.style.display = "none";
			fixedLowerArea.style.display = "none";
		} else if (type == 3) {
			wholeArea.innerHTML = "";
			variableUpperArea.innerHTML = "";
			variableLowerArea.innerHTML = "";
			wholeArea.style.display = "none";
			variableUpperArea.style.display = "none";
			variableLowerArea.style.display = "none";
			fixedUpperArea.style.display = "block";
			fixedLowerArea.style.display = "block";
		}
	}

	function diplayStudentInfo() {
		displayArea(1);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	wholeArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send();
	}

	function diplayFormUpdateStudentInfo() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	wholeArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentFormUpdateStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send();
	}

	function updateStudentInfo() {
		var phone = document.getElementById("phone").value;
		var address = document.getElementById("address").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	diplayStudentInfo();
		    }
		  };
		  xhttp.open("POST", "studentFormUpdateStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("phone=" + phone + "&address=" + address); 
	}

	function displaySelectionSemInStudentEvaluation() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	variableUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectSemInStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displaySelectionStudentEvaluationWholeStudy() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	variableLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectStudentEvaluationWholeStudy.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayStudentEvaluationBySemList() {
		var sem_id = document.getElementById("semId").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	variableLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectStudentEvaluationBySem.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_id=" + sem_id); 
	}

	function displayStudentEvaluationBySem(sem_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	variableLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectStudentEvaluationBySem.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_id=" + sem_id); 
	}

	function displayStudentEvaluationByCourse(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	variableLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectStudentEvaluationByCourse.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id); 
	}
	
	function displayStudentEvaluation() {
		displayArea(2);
		displaySelectionSemInStudentEvaluation();
		displaySelectionStudentEvaluationWholeStudy();
	}

	function displayStudentSubjectList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentSelectStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayFormInsertStudentSubject() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentFormInsertStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function insertStudentSubject() {
		var sub_id = document.getElementById("sub_id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayFormInsertStudentSubject();
		    	displayStudentSubjectList();
		    }
		  };
		  xhttp.open("POST", "studentInsertStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id); 
	}

	function displayFormUpdateStudentSubject(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "studentFormUpdateStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id); 
	}

	function updateStudentSubject(sub_stu_id) {
		var sub_id = document.getElementById("sub_id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayFormInsertStudentSubject();
		    	displayStudentSubjectList();
		    }
		  };
		  xhttp.open("POST", "studentUpdateStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id + "&sub_stu_id=" + sub_stu_id); 
	}

	function deleteStudentSubject(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
				displayStudentSubjectList();
		    }
		  };
		  xhttp.open("POST", "studentDeleteStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id); 
	}
	
	function displayStudentSubject() {
		displayArea(3);
		displayStudentSubjectList();
		displayFormInsertStudentSubject();
	}
	
</script>


	<div class="container-fluid.mt-0"
		style="height: 864px; background-color: Goldenrod;">
		<div id="header" style="height: 10%">
			<nav class="navbar navbar-expand-sm bg-light navbar-light">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="">Student
						of Reminthink University</a></li>
				<li class="nav-item"><a class="nav-link" href="javascript:diplayStudentInfo();">Student
						Information</a></li>
				<li class="nav-item"><a class="nav-link" href="javascript:displayStudentSubject();">Select
						Subject</a></li>
				<li class="nav-item"><a class="nav-link" href="javascript:displayStudentEvaluation();">Course
						Evaluation</a></li>
			</ul>
			</nav>
		</div>
		<div id="content" style="height: 85%;">
			<div
				style="background-image: url('img/img_0001.jpg'); height: 100%; background-size: cover; background-repeat: no-repeat; background-position: center;">
				<div style="height: 100%; background-color: rgba(250, 230, 230, 0.70); ">
					<div id="wholeArea" style="height: 100%; overflow-y: scroll; padding: 10px 10px 10px 10px;">
						<h1 align="center">Welcome to Reminthink University Management System (Student Version)</h1>
					</div>
					<div id="variableUpperArea" style="padding: 10px 10px 10px 10px;"></div>
					<div id="variableLowerArea" style="padding: 10px 10px 10px 10px;"></div>
					<div id="fixedUpperArea" style="height: 60%; overflow-y: scroll; padding: 10px 10px 10px 10px;"></div>
					<div id="fixedLowerArea" style="height: 40%; overflow-y: scroll; padding: 10px 10px 10px 10px;"></div>
				</div>
				
			</div>
		</div>
		<div id="footer" style="height: 5%;"><jsp:include page="footer.jsp"></jsp:include></div>
	</div>
	<script type="text/javascript">displayArea(1);</script>
	
	
</body>
</html>