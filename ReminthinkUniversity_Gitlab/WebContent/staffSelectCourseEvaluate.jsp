<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
	Lecturer staff = new Lecturer();
	Connection connection = staff.databaseConnection();
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	staff.selectCourseEvaluate(connection, sub_id);
	float totalMarks = 0;
%>
<h4>Criteria of Evaluation For</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>Method</td>
			<td>Weightage</td>
			<td>Details</td>
		</tr>
		
		<%
		for (int i = 0; i < staff.getMeName().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getMeName().get(i) + "</td>");
			out.println("<td>" + staff.getEvWeightage().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-info' onclick='displayStudentEvaluationByCriteria(" + staff.getEvId().get(i) + ")'>Details</button></td>");
			totalMarks += staff.getEvWeightage().get(i);
		}
		%>
		<tr>
			<td colspan='2'><strong>Total: </strong></td>
			<td colspan='2'><strong><%=totalMarks %></strong></td>
		</tr>
	</tbody>
</table>

<button type="button" class="btn btn-primary" onclick="displayArea(2);">Back to Course Record and Evaluation</button>