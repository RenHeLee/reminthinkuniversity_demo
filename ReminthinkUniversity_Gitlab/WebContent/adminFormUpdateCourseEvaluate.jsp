<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();

	int ev_id = Integer.parseInt(request.getParameter("ev_id"));
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	int me_id = Integer.parseInt(request.getParameter("me_id"));
	float ev_weightage = Float.parseFloat(request.getParameter("ev_weightage"));

	if (ev_id != 0 && sub_id != 0 && me_id != 0 && ev_weightage != 0){
		staff.updateCourseEvaluate(connection, sub_id, me_id, ev_weightage, sta_id, ev_id);
	}
	

%>
<h4>Update course evaluate</h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Subject:</td>
				<td><select id="sub_id">
						<%
							staff.selectCourseEvaluate(ev_id, connection);
							for (int i = 0; i < staff.getMeId().size(); i++) {
								out.println(
										"<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + "(Section " + staff.getSec().get(i) + ")" + "</option>");
							}
							staff.selectAllSubjectBySection(connection);
							for (int i = 0; i < staff.getSubCo().size(); i++) {
								out.println(
										"<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + "(Section " + staff.getSec().get(i) + ")" + "</option>");
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Method</td>
				<td><select id="me_id">
						<%
						staff.selectCourseEvaluate(ev_id, connection);
						for (int i = 0; i < staff.getMeId().size(); i++) {
							out.println(
									"<option value=" + staff.getMeId().get(i) + ">" + staff.getMeName().get(i) + "</option>");
						}
						staff.selectAllEvaluateMethod(connection);
						for (int i = 0; i < staff.getMeId().size(); i++) {
							out.println(
									"<option value=" + staff.getMeId().get(i) + ">" + staff.getMeName().get(i) + "</option>");
						}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Weightage</td>
				<td><input type="number" required id=ev_weightage min="0" value="<% 
				
				staff.selectCourseEvaluate(ev_id, connection);
				for (int i = 0; i < staff.getMeId().size(); i++) 
				{out.print(staff.getEvWeightage().get(i));}
				
				%>" step=".01"></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="updateCourseEvaluate(<%=ev_id%>)">Update</button>
	<button type="button" class="btn btn-danger" onclick="displayInsertCourseEvaluate()">Discard</button>
</form>
