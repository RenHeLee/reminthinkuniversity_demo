<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();
int sub_stu_id = Integer.parseInt(request.getParameter("sub_stu_id"));
int ev_id = Integer.parseInt(request.getParameter("ev_id"));
float marks = Float.parseFloat(request.getParameter("marks"));

if(staff.checkStudentEvaluation(connection, sub_stu_id, ev_id)){
	out.println(staff.insertStudentEvaluation(connection, sub_stu_id, ev_id, marks, sta_id));
}else{
	out.println("Repeated");
}

%>
