<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<script type="text/javascript">
<% 
String message = "";
try {
	message = request.getParameter("message");
	if (!message.isEmpty()){
		out.println("window.alert('" + message + "');");
	}
}catch(NullPointerException ex){ 
	
}


%>

</script>
<title>Reminthink University Portal</title>
</head>
<body>
	<div class="container-fluid.mt-0"
		style="height: 100%; background-color: Goldenrod;">
		<div id="header" style="height: 10%">
			<nav class="navbar navbar-expand-sm bg-light navbar-light">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="#">Reminthink University</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">About</a></li>
				<li class="nav-item"><a class="nav-link" href="http://reminthink.000webhostapp.com/">About Reminthink</a></li>
				</ul>
			</nav>
		</div>
		<div id="content" style="height: 85%;">
			<div
				style="background-image: url('img/img_0001.jpg'); height: 100%; background-size: cover; background-repeat: no-repeat; background-position: center;">
				<div
					style="background-color: rgba(230, 250, 230, 0.70); height: 100%; width: 100%;">
					<h1>Login</h1>
					<form action="login.jsp" method="post">
						<table class="table table-hover">
							<tbody>
								<tr>
									<td>ID:</td>
									<td><input type="text" id="id" name="id"></td>
								</tr>
								<tr>
									<td>Password:</td>
									<td><input type="password" id="password" name="password"></td>
								</tr>
								<tr>
									<td><input type="submit" name="loginForm"
										class="btn btn-outline-success"></td>
									<td><input type="reset" class="btn btn-outline-danger"></td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div id="footer" style="height: 5%;"><jsp:include page="footer.jsp"></jsp:include></div>
	</div>
</body>
</html>