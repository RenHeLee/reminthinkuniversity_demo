<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

int stu_id = Integer.parseInt(request.getParameter("stu_id"));

if(staff.deleteStudentInfo(connection, stu_id)){
	out.print("<p>This student's record is deleted successfully.</p><button type='button' class='btn btn-info' onclick='displayInsertStudentForm()'>Back</button>");
}else{
	out.print("<p>This student's record cannot be deleted.</p><button type='button' class='btn btn-info' onclick='displayInsertStudentForm()'>Back</button>");
}


%>