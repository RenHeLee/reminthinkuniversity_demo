<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
	Lecturer staff = new Lecturer();
	Connection connection = staff.databaseConnection();
	String successMessage = "";
	String errorMessage = "";
	
	int sub_id = 0;
	int sub_stu_id = Integer.parseInt(request.getParameter("sub_stu_id"));
	int ev_id = Integer.parseInt(request.getParameter("ev_id"));
	float marks = Float.parseFloat(request.getParameter("marks"));
	int input_type = Integer.parseInt(request.getParameter("input_type")); // 1: input form by sub_stu_id; 2: input form by ev_id;
	if((sub_stu_id == 0 ^ ev_id == 0) && marks == 0){
		
	}else if (sub_stu_id != 0 && ev_id != 0 && marks != 0){
		if(staff.checkStudentEvaluation(connection, sub_stu_id, ev_id)){
			if(staff.insertStudentEvaluation(connection, sub_stu_id, ev_id, marks, sta_id)){
				successMessage = "This student's evaluation record is inserted successfully." + sub_stu_id + "@" + ev_id + "@" + marks;
			}else{
				errorMessage = "This student's evaluation record is failed to insert.";
			}
		}else{
			errorMessage = "This student's evaluation record is repeated.";
		}
	}else if(marks == 0){
		errorMessage = "Please ensure all column are not empty.";
	}
	if(input_type == 1){
		ev_id = 0;
		sub_id = staff.selectSubjectIdByStudent(connection, sub_stu_id);
	}else if(input_type == 2){
		sub_stu_id = 0;
		sub_id = staff.selectSubjectIdByCourseEvaluate(connection, ev_id);
	}
%>
<h4>Insert Evaluation</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
	
		<%
		if(sub_stu_id == 0){
			out.println("Student name: <select id='sub_stu_id'><option value=''>Select student</option>");
			staff.selectStudentSubjectList(connection, sub_id);
				for (int i = 0; i < staff.getSubStuId().size(); i++) {
					out.println("<option value=" + staff.getSubStuId().get(i) + ">" + staff.getNameList().get(i) + " (S"
							+ staff.getIdList().get(i) + ")" + "</option>");
				}
		}
		
		%>
	</select> 
		<%
		if(ev_id == 0){
			out.println("Evaluation: <select id = 'ev_id'><option value=''>Select evaluation method</option>");
			staff.selectCourseEvaluate(connection, sub_id);
			for (int i = 0; i < staff.getEvId().size(); i++) {
				out.println("<option value=" + staff.getEvId().get(i) + ">" + staff.getMeName().get(i) + staff.getEvId().get(i) + "(" + staff.getEvWeightage().get(i) + ")" + "</option>");
			}
		}
		
		%>
	</select>
	Marks: <input type="number" required id="marks" min="0" value="<%=staff.getMarks() %>" step=".01">
	
	
	<button type="button" class="btn btn-success"
		onclick="<%
			if(input_type == 1){
				out.println("insertStudentEvaluationByStudent(" + sub_stu_id +")");
			}else if(input_type == 2){
				out.println("insertStudentEvaluationByCriteria(" + ev_id +")");
			}
			
			%>">Insert</button>
	<input type="reset" value="Reset" class="btn btn-warning">
</form>
