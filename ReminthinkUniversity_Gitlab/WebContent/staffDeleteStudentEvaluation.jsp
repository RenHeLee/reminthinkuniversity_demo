<%@page import="com.project.database.Lecturer"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Lecturer staff = new Lecturer();
Connection connection = staff.databaseConnection();
int stu_ev_id = Integer.parseInt(request.getParameter("stu_ev_id"));
int id = Integer.parseInt(request.getParameter("id"));
int input_type = Integer.parseInt(request.getParameter("input_type")); // 1: input form by sub_stu_id; 2: input form by ev_id;
if(staff.deleteStudentEvaluation(connection, stu_ev_id)){
	out.print("<p>This student's evaluation record is deleted successfully.</p>");
}else{
	out.print("<p>This student's evaluation record cannot be deleted.</p>");
}

%>

<button type='button' class='btn btn-info' onclick='<%
	if(input_type == 1){
		out.println("displayFormInsertStudentEvaluationByStudent(" + id +")");
	}else if(input_type == 2){
		out.println("displayFormInsertStudentEvaluationByCritetia(" + id +")");
	}
	
	%>'>Back</button>