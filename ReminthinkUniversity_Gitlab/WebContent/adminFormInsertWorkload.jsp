<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

String successMessage = "";
String errorMessage = "";
int sub_id = Integer.parseInt(request.getParameter("sub_id"));
int id = Integer.parseInt(request.getParameter("id"));

if(sub_id == 0 && id == 0){
}else if (sub_id != 0 && id != 0){
	if(staff.checkWorkload(connection, sub_id, id)){
		staff.insertWorkload(connection, sub_id, id, sta_id);
		successMessage = "This record is inserted successfully.";
	}else{
		errorMessage = "Please ensure record is unique.";
	}
}else{
	errorMessage = "Please ensure all column are not empty.";
}

%>

<h4>Insert workload</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Lecturer:</td>
				<td><select id="id">
						<%
							staff.selectAllStaffInfo(connection);
							for (int i = 0; i < staff.getIdList().size(); i++) {
								out.println(
										"<option value='" + staff.getIdList().get(i) + "'>" + staff.getNameList().get(i) + "</option>");
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Subject and section:</td>
				<td><select id="sub_id">
						<%
							staff.selectAllSubjectBySection(connection);
							for (int i = 0; i < staff.getSubId().size(); i++) {
								out.println("<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + " - Section "
										+ staff.getSec().get(i) + "(" + staff.getSemCo().get(i) + ")" + "</option>");
							}
						%>
				</select></td>
			</tr>
		</tbody>
	</table>
<button type="button" class="btn btn-success" onclick="insertWorkload();">Insert</button>
<input type="reset" value="Reset" class="btn btn-warning" >
</form>
