<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
	Lecturer staff = new Lecturer();
	Connection connection = staff.databaseConnection();
	int stu_ev_id = Integer.parseInt(request.getParameter("stu_ev_id"));
	staff.selectStudentEvaluationByStudentEvaluateId(connection, stu_ev_id);
	int sub_id = staff.getSubId().get(0);
	float marks = Float.parseFloat(request.getParameter("marks"));
	int id = Integer.parseInt(request.getParameter("id"));
	String successMessage = "";
	int input_type = Integer.parseInt(request.getParameter("input_type")); // 1: input form by sub_stu_id; 2: input form by ev_id;
	if(marks == 0){
		
	}else if (marks != 0){
		staff.updateStudentEvaluation(connection, stu_ev_id, marks, sta_id);
		successMessage = "Student's evaluation is updated successfully";
	}
	
%>
<h4>Update <%
		staff.selectStudentEvaluationByStudentEvaluateId(connection, stu_ev_id);
			out.println(staff.getMeName().get(0));
		%> Evaluation of <%=staff.getNameList().get(0) %></h4>
		<% if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Error: </strong>" + successMessage + "</div>");
} %>
<form>
	Marks: <input type="number" required id="marks" min="0" value="<%=staff.getMarks().get(0) %>" step=".01">
	
	
	<button type="button" class="btn btn-success"
		onclick="updateStudentEvaluation(<%=stu_ev_id %>, <%=id %>, <%=input_type %>)">Update</button>
	<button type="button" class="btn btn-danger"
		onclick="<% 
				if(input_type == 1){
					out.println("displayFormInsertStudentEvaluationByStudent(" + id +")");
				}else if(input_type == 2){
					out.println("displayFormInsertStudentEvaluationByCritetia(" + id +")");
				}
		%>">Discard</button>
</form>
