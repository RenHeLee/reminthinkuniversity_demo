<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
	Student student = new Student();
	Connection connection = student.databaseConnection();
	int sub_stu_id = Integer.parseInt(request.getParameter("sub_stu_id"));
	student.selectStudentSubjectBySubStuId(connection, sub_stu_id);
%>
<h4>Update Subject 
<%
	for (int i = 0; i < student.getSubId().size(); i++){
		out.println(student.getSubName().get(i) + " - Section " + student.getSec().get(i) + "(" + student.getSemCo().get(i) + ")");
	}
%>
</h4>
<form>
	<p>Subject and section:</p>
	<select id = "sub_id">
	<%
				for (int i = 0; i < student.getSubId().size(); i++){
					out.println("<option value="+ student.getSubId().get(i) +">" + student.getSubName().get(i) + " - Section " + student.getSec().get(i) + "(" + student.getSemCo().get(i) + ")" + "</option>");
				}
				
				student.selectAllSubjectBySection(connection);
				for (int i = 0; i < student.getSubId().size(); i++){
					out.println("<option value="+ student.getSubId().get(i) +">" + student.getSubName().get(i) + " - Section " + student.getSec().get(i) + "(" + student.getSemCo().get(i) + ")" + "</option>");
				}
				
				%>
	</select>
	</td>
	<button type="button" class="btn btn-success"
		onclick="updateStudentSubject(<%=sub_stu_id %>)">Update</button>
	<input type="reset" class="btn btn-warning" value="Reset" />
	<button type="button" class="btn btn-danger"
		onclick="displayFormInsertStudentSubject()">Discard</button>
</form>