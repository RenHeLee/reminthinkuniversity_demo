<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

staff.selectStudentSubject(connection);
%>
<h4>All student subject</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No</td>
			<td>Student</td>
			<td>Subject</td>
			<td>Section</td>
			<td>Semester</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		<%
		for (int i = 0; i < staff.getNameList().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getNameList().get(i) + "(S" + staff.getIdList().get(i) + ")</td>");
			out.println("<td>" + staff.getSubName().get(i) + "</td>");
			out.println("<td>" + staff.getSec().get(i) + "</td>");
			out.println("<td>" + staff.getSemCo().get(i) + "</td>");
			if(staff.getModBy().get(i).equalsIgnoreCase("()")){
				out.println("<td>" + staff.getNameList().get(i) + "(S" + staff.getIdList().get(i)+ ")</td>");
			}else{
				out.println("<td>" + staff.getModBy().get(i) + "</td>");
			}
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayUpdateSubjectStudentForm(" + staff.getSubStuId().get(i) + ")'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteSubjectStudent(" + staff.getSubStuId().get(i) + ")'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>