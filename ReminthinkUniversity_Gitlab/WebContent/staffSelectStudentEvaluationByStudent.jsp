<%@page import="com.project.database.Lecturer"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Lecturer staff = new Lecturer();
Connection connection = staff.databaseConnection();
int sub_stu_id = Integer.parseInt(request.getParameter("sub_stu_id"));
staff.selectStudentSubjectBySubStuId(connection, sub_stu_id);
%>

<h4><%out.println(staff.getNameList().get(0) + " (S"
		+ staff.getIdList().get(0) + ")'s " + staff.getSubName().get(0) + " - Section "
		+ staff.getSec().get(0) + "(" + staff.getSemCo().get(0) + ")" );%>&emsp;&emsp;&emsp;<button type="button" class="btn btn-primary" onclick="displayArea(1);">Back to Overall of Evaluation</button></h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>Evaluate method</td>
			<td>Marks</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		<%
		staff.selectStudentEvaluationByStudent(connection, sub_stu_id);
		for (int i = 0; i < staff.getMeName().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getMeName().get(i) + "</td>");
			out.println("<td>" + staff.getMarks().get(i) + "</td>");
			out.println("<td>" + staff.getModBy().get(i)  + "</td>");
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayFormUpdateStudentEvaluation(" + staff.getStuEvId().get(i) + ", " + sub_stu_id + ", 1)'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteStudentEvaluation(" + staff.getStuEvId().get(i) + ", " + sub_stu_id + ", 1)'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>
<h4>Total marks: <%=staff.getTotalMark() %></h4>
