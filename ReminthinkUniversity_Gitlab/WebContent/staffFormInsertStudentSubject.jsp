<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();
	
	String successMessage = "";
	String errorMessage = "";
	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	int stu_id = Integer.parseInt(request.getParameter("stu_id"));

	
	if(sub_id == 0 && stu_id == 0){
	}else if (sub_id != 0 && stu_id != 0){
		if(staff.checkStudentSubject(connection, sub_id, stu_id)){
			staff.insertStudentSubject(connection, sub_id, stu_id, sta_id);
			successMessage = "This student's record is inserted successfully.";
		}else{
			errorMessage = "This record is repeated.";
		}
	}else if(sub_id != 0 ^ stu_id != 0){
		errorMessage = "Please ensure all column are not empty.";
	}
%>
<h4>Insert Student Subject</h4>
<%
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
	Student name: <select id="stu_id">
	<option value="0">Please select student name</option>
		<%
			staff.selectAllStudentInfo(connection);
			for (int i = 0; i < staff.getIdList().size(); i++) {
				out.println("<option value=" + staff.getIdList().get(i) + ">" + staff.getNameList().get(i) + " (S"
						+ staff.getIdList().get(i) + ")" + "</option>");
			}
		%>
	</select> Subject and section: <select id="sub_id">
	<option value="0">Please select subject and section</option>
		<%
			staff.selectAllSubjectBySection(connection);
			for (int i = 0; i < staff.getSubId().size(); i++) {
				out.println("<option value=" + staff.getSubId().get(i) + ">" + staff.getSubName().get(i) + " - Section "
						+ staff.getSec().get(i) + "(" + staff.getSemCo().get(i) + ")" + "</option>");
			}
		%>
	</select>
	<button type="button" class="btn btn-success"
		onclick="insertSubjectStudent()">Insert</button>
	<input type="reset" class="btn btn-info" value="Reset" />
</form>
