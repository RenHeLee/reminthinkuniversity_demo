<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

staff.selectAllSemDate(connection);

%>

<h4>Semester date information</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>Semester</td>
			<td>Starting from</td>
			<td>Until to</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		<%
		for (int i = 0; i < staff.getSemCo().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getSemCo().get(i) + "</td>");
			out.println("<td>" + staff.getFirDate().get(i) + "</td>");
			out.println("<td>" + staff.getLasDate().get(i) + "</td>");		
			out.println("<td>" + staff.getModBy().get(i) + "</td>");
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayUpdateSemDate(" + staff.getSemId().get(i) + ")'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteSemDate(" + staff.getSemId().get(i) + ")'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>