<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();

	int sub_id = Integer.parseInt(request.getParameter("sub_id"));
	int sub_co = Integer.parseInt(request.getParameter("sub_co"));
	int sec = Integer.parseInt(request.getParameter("sec"));
	int sem_id = Integer.parseInt(request.getParameter("sem_id"));

	;
	
	if (sub_co != 0 && sec != 0 && sem_id != 0){
		staff.updateSubjectBySection(connection, sub_co, sec, sem_id, sta_id, sub_id);
	}
%>
<h4>Update subject by section</h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Subject:</td>
				<td><select id="sub_co" form="sub_co">
						<%
							staff.selectSubjectBySection(connection, sub_id);
							for (int i = 0; i < staff.getSubCo().size(); i++) {
								out.println(
										"<option value='" + staff.getSubCo().get(i) + "'>" + staff.getSubName().get(i) + "</option>");
							}
							staff.selectAllSubject(connection);
							for (int i = 0; i < staff.getSubCo().size(); i++) {
								out.println(
										"<option value='" + staff.getSubCo().get(i) + "'>" + staff.getSubName().get(i) + "</option>");
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Section</td>
				<td><input type="number" id="sec" min=1 value="<%
				staff.selectSubjectBySection(connection, sub_id);
				out.print(staff.getSec().get(0)); 
				%>"></td>
			</tr>
			<tr>
				<td>Semester:</td>
				<td><select id="sem_id" form="sem_id">
						<%
						staff.selectSubjectBySection(connection, sub_id);
						for (int i = 0; i < staff.getSemId().size(); i++) {
							out.println(
									"<option value='" + staff.getSemId().get(i) + "'>" + staff.getSemCo().get(i) + "</option>");
						}
						staff.selectAllSemDate(connection);
						for (int i = 0; i < staff.getSemId().size(); i++) {
							out.println(
									"<option value='" + staff.getSemId().get(i) + "'>" + staff.getSemCo().get(i) + "</option>");
						}
						%>
				</select></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="updateSubjectBySection(<%=sub_id%>)">Update</button>
	<button type="button" class="btn btn-danger" onclick="displayInsertSubjectBySection()">Discard</button>
</form>
