<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
Student student = new Student();
Connection connection = student.databaseConnection();
student.selectStudentInfo(connection, stu_id);
%>
<h4>Student Information of <%=student.getName() %></h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>ID: </td>
			<td><% out.println(student.getPo_co() + stu_id); %></td>
		</tr>
		<tr>
			<td>Name: </td>
			<td><%=student.getName() %></td>
		</tr>
		<tr>
			<td>Intake Semester: </td>
			<td><%=student.getIntake_sem_co()%></td>
		</tr>
		<tr>
			<td>Graduate Semester: </td>
			<td><%=student.getGraduate_sem_co()%></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><%=student.getPhone() %></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><%=student.getIc() %></td>
		</tr>
		<tr>
			<td>Birthday: </td>
			<td><%
			String birthday = student.getIc();
			String strYear = birthday.substring(0,1);
			Integer year = Integer.parseInt(strYear);
			
			if(year.compareTo(Integer.parseInt("4")) > 0){
				out.println("19" + birthday.substring(0,2) + "-" + birthday.substring(2,4) + "-" + birthday.substring(4,6));
			}else{
				out.println("20" + birthday.substring(0,2) + "-" + birthday.substring(2,4) + "-" + birthday.substring(4,6));
			}
			
			%></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><%=student.getAddress() %></td>
		</tr>
		<tr>
			<td>Email: </td>
			<td><% out.println(student.getPo_co().toLowerCase() + stu_id + "@ru.edu.my"); %></td>
		</tr>
		<tr>
			<td>Modified by: </td>
			<td><%
			out.println(student.getMod_by());
			%></td>
		</tr>
		<tr>
			<td>Modified date: </td>
			<td><%=student.getMod_date() %></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-warning" onclick="diplayFormUpdateStudentInfo();">Update</button></td>
