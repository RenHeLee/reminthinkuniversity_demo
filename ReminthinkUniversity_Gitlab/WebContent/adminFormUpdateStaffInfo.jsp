<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();




String sta_name = request.getParameter("sta_name");
int sta_po = Integer.parseInt(request.getParameter("sta_po"));
String firstDate = request.getParameter("fir_ser_date");
String lastDate = request.getParameter("las_ser_date");
Date fir_ser_date = null;
Date las_ser_date = null;
if(firstDate != ""){
	fir_ser_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("fir_ser_date"));
}
if(lastDate != ""){
	las_ser_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("las_ser_date"));
}
String pho_num = request.getParameter("pho_num");
String ic_num = request.getParameter("ic_num");
String address = request.getParameter("address");
int id = Integer.parseInt(request.getParameter("sta_id"));

staff.selectStaffInfo(connection, id);

if (sta_name != "" && sta_po != 0 && firstDate != "" && firstDate != "" && pho_num  != "" && ic_num != "" && address != ""){
	staff.updateStaffInfo(connection, sta_name, sta_po, fir_ser_date, las_ser_date, pho_num, ic_num, address, sta_id, id);
}
%>
<h4>Update <%=staff.getPo_title()%> information</h4>
<form>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>ID: </td>
			<td><input type="text" id="sta_id" value="<% out.println(staff.getPo_co() + id); %>" disabled="disabled"></td>
		</tr>
		<tr>
			<td>Name: </td>
			<td><input type="text" id="sta_name" value="<%=staff.getName() %>"></td>
		</tr>
		<tr>
			<td>Position: </td>
			<td>
				<select id="sta_po" form="sta_po">
				  
				  <%
				  out.println("<option value='" + staff.getPo_id() + "'>" + staff.getPo_title() + "</option>");
				  	staff.selectAllPosition(connection);
				  	for(int i = 0; i < staff.getPoTitle().size(); i++){
				  		out.println("<option value='" + staff.getPoId().get(i) + "'>" + staff.getPoTitle().get(i) + "</option>");
				  	}
				  %>
				</select>

			</td>
		</tr>
		<tr>
			<td>First service date: </td>
			<td><input type="date" id="fir_ser_date" value="<%=staff.getFir_ser_date()%>"></td>
		</tr>
		<tr>
			<td>Last service date: </td>
			<td><input type="date" id="las_ser_date" value="<%=staff.getLas_ser_date()%>"></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><input type="text" id="pho_num" value="<%=staff.getPhone() %>"></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><input type="text" id="ic_num" value="<%=staff.getIc() %>" disabled="disabled"></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><input type="text" id="address" value="<%=staff.getAddress() %>"></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-info" onclick="updateStaffInformation(<%=id %>)">Update</button>
<button type="button" class="btn btn-info" onclick="displayInsertStaffInformation()">Discard</button>
</form>
