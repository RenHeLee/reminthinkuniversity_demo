<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
Student student = new Student();
Connection connection = student.databaseConnection();

int sem_id = Integer.parseInt(request.getParameter("sem_id"));
String sem_co = student.selectSemCode(connection, sem_id);
student.selectStudentEvaluationBySem(connection, stu_id, sem_id);
%>

<h3><%=sem_co %></h3>
<table class="table table-hover">
	<tbody>
		<tr>
			<td style="width: 5%;">No.</td>
			<td style="width: 15%;">Subject name</td>
			<td style="width: 15%;">Section</td>
			<td style="width: 15%;">Marks</td>
			<td style="width: 15%;">Details</td>
		</tr>
		<%
		for (int i = 0; i < student.getSubName().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + student.getSubName().get(i) + "</td>");
			out.println("<td>" + student.getSec().get(i) + "</td>");
			out.println("<td>" + student.getMarks().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-info' onclick='displayStudentEvaluationByCourse(" + student.getSubStuId().get(i) + ")'>Details</button></td></tr>");
		}
		%>
	</tbody>
</table>