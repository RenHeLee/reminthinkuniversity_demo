<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

int stu_id = Integer.parseInt(request.getParameter("stu_id"));

staff.selectStudentInfo(connection, stu_id);

%>

<h4>Student Information of <%=staff.getName() %></h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>ID: </td>
			<td><% out.println(staff.getPo_co() + stu_id); %></td>
		</tr>
		<tr>
			<td>Name: </td>
			<td><%=staff.getName() %></td>
		</tr>
		<tr>
			<td>Intake Semester: </td>
			<td><%=staff.getIntake_sem_co()%></td>
		</tr>
		<tr>
			<td>Graduate Semester: </td>
			<td><%=staff.getGraduate_sem_co()%></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><%=staff.getPhone() %></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><%=staff.getIc() %></td>
		</tr>
		<tr>
			<td>Birthday: </td>
			<td><%
			String birthday = staff.getIc();
			String strYear = birthday.substring(0,2);
			Integer year = Integer.parseInt(strYear);
			
			if(year.compareTo(Integer.parseInt("4")) > 0){
				out.println("19" + birthday.substring(0,2) + "-" + birthday.substring(2,4) + "-" + birthday.substring(4,6));
			}else{
				out.println("20" + birthday.substring(0,2) + "-" + birthday.substring(2,4) + "-" + birthday.substring(4,6));
			}
			
			%></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><%=staff.getAddress() %></td>
		</tr>
		<tr>
			<td>Email: </td>
			<td><a href="mailto:<% out.println(staff.getPo_co().toLowerCase() + stu_id + "@ru.edu.my"); %>"><% out.println(staff.getPo_co().toLowerCase() + stu_id + "@ru.edu.my"); %><a></a></td>
		</tr>
		<tr>
			<td>Modified by: </td>
			<td><%
			out.println(staff.getMod_by());
			%></td>
		</tr>
		<tr>
			<td>Modified date: </td>
			<td><%=staff.getMod_date() %></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-success" onclick="displayInsertStudentForm()">Discard</button></td>
<button type="button" class="btn btn-warning" onclick="displayUpdateStudentForm(<%= stu_id%>)">Update</button></td>
<button type="button" class="btn btn-danger" onclick="deleteStudentInformationByStudentId(<%= stu_id%>)">Delete</button></td>