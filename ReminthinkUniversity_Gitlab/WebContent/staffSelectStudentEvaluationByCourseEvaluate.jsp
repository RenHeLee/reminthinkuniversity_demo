<%@page import="com.project.database.Lecturer"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Lecturer staff = new Lecturer();
Connection connection = staff.databaseConnection();
int ev_id = Integer.parseInt(request.getParameter("ev_id"));

staff.selectStudentEvaluationByCourseEvaluate(connection, ev_id);
%>

<h4>Evaluation Record of Xxxx in Criteria Yyyy &emsp;&emsp;&emsp;<button type="button" class="btn btn-primary" onclick="displayArea(1);">Back to Criteria of Evaluation</button></h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>Student</td>
			<td>Matric number</td>
			<td>Marks</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		<%
		for (int i = 0; i < staff.getNameList().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getNameList().get(i) + "</td>");
			out.println("<td>S" + staff.getIdList().get(i) + "</td>");
			out.println("<td>" + staff.getMarks().get(i) + "</td>");
			out.println("<td>" + staff.getModBy().get(i)  + "</td>");
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayFormUpdateStudentEvaluation(" + staff.getStuEvId().get(i) + "," + ev_id + ",2);'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteStudentEvaluation(" + staff.getStuEvId().get(i) + ", " + ev_id + ", 2)'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>
