<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
	Student student = new Student();
	Connection connection = student.databaseConnection();
	student.selectAllSubjectBySection(connection);
%>
<h4>Insert subject</h4>
<form>
	<p>Subject and section:</p>
	<select id = "sub_id">
	<%
				for (int i = 0; i < student.getSubId().size(); i++){
					out.println("<option value="+ student.getSubId().get(i) +">" + student.getSubName().get(i) + " - Section " + student.getSec().get(i) + "(" + student.getSemCo().get(i) + ")" + "</option>");
				}
				
				%>
	</select>
	<button type="button" class="btn btn-success"
		onclick="insertStudentSubject()">Insert</button>
</form>