<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
	Admin staff = new Admin();
	Connection connection = staff.databaseConnection();
	int lecId = 0;
	lecId = Integer.parseInt(request.getParameter("lecId"));
	staff.selectStaffInfo(connection, sta_id);
	int semId = 0;
	if (request.getParameter("semId") != null) {
		semId = Integer.parseInt(request.getParameter("semId"));
		staff.selectWorkload(connection, lecId, semId);
	} else {
		staff.selectWorkload(connection, lecId);
	}
%>
<h4>Workload</h4>
<table class="table tabfle-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<%
			if(semId == 0){
				out.println("<td>Semester</td>");
			}
			%>
			<td>Subject</td>
			<td>Section</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		<%

		for(int i = 0; i < staff.getSec().size(); i++){
			out.println("<tr><td>" + (i + 1) + "</td>");
			if(semId == 0){
				out.println("<td><a href='javascript:selectSemInWorkload(" + staff.getSemId().get(i) + ")'>" + staff.getSemCo().get(i) + "</td>");
			}
			out.println("<td>" + staff.getSubName().get(i) + "</td>");
			out.println("<td>" + staff.getSec().get(i) + "</td>");
			out.println("<td>" + staff.getModBy().get(i) + "</td>");
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' onclick='updateWorkload(" + staff.getWoId() + ")'>Update</button></td>");
			out.println("<td><button type='button' onclick='deleteWorkload(" + staff.getWoId() + ")'>Delete</button></td>");
		}
		%>
	</tbody>
</table>
