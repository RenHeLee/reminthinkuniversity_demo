<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Lecturer staff = new Lecturer();
Connection connection = staff.databaseConnection();

staff.selectStaffInfo(connection, sta_id);
%>
<h4><%=staff.getPo_title().substring(0, 1).toUpperCase() + staff.getPo_title().substring(1) %> information</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>ID: </td>
			<td><% out.println(staff.getPo_co() + sta_id); %></td>
		</tr>
		<tr>
			<td>Name: </td>
			<td><%=staff.getName() %></td>
		</tr>
		<tr>
			<td>First service date: </td>
			<td><%=staff.getFir_ser_date()%></td>
		</tr>
		<tr>
			<td>Last service date: </td>
			<td><%=staff.getLas_ser_date()%></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><%=staff.getPhone() %></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><%=staff.getIc() %></td>
		</tr>
		<tr>
			<td>Birthday: </td>
			<td><%
			String birthday = staff.getIc();
			String strYear = birthday.substring(0,1);
			Integer year = Integer.parseInt(strYear);
			
			if(year.compareTo(Integer.parseInt("1")) > 0){
				out.println("19" + birthday.substring(0,2) + "-" + birthday.substring(2,4) + "-" + birthday.substring(4,6));
			}else{
				out.println("20" + birthday.substring(0,2) + "-" + birthday.substring(2,4) + "-" + birthday.substring(4,6));
			}
			
			%></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><%=staff.getAddress() %></td>
		</tr>
		<tr>
			<td>Email: </td>
			<td><% out.println(staff.getPo_co().toLowerCase() + sta_id + "@ru.edu.my"); %></td>
		</tr>
		<tr>
			<td>Modified by: </td>
			<td><%
			out.println(staff.getMod_by());
			 %></td>
		</tr>
		<tr>
			<td>Modified date: </td>
			<td><%=staff.getMod_date() %></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-info" onclick="diplayFormUpdateLecturerInfo()">Update</button></td>