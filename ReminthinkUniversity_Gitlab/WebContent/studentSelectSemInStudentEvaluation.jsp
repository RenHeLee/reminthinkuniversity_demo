<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
Student student = new Student();
Connection connection = student.databaseConnection();

student.selectSemInStudentEvaluation(connection, stu_id);

%>
<h4>Course Evaluation</h4>
<form>
	Semester:
	<select id='semId'>
	<%
				for (int i = 0; i < student.getSemId().size(); i++){
					out.println("<option value="+ student.getSemId().get(i) +">" + student.getSemCo().get(i) + "</option>");
				}
				%>
	</select>
	<button type="button" class="btn btn-success"
		onclick="displayStudentEvaluationBySemList()">Select</button>
	<button type="button" class="btn btn-info"
		onclick="displaySelectionStudentEvaluationWholeStudy()">Whole Study</button>
</form>