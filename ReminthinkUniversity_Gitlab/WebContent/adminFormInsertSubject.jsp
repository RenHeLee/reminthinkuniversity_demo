<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

String sub_name = request.getParameter("sub_name");
String successMessage = "";
String errorMessage = "";
if(sub_name != ""){
	if(staff.checkSubject(connection, sub_name)){
		staff.insertSubject(connection, sub_name, sta_id);
		successMessage = "This subject name is inserted successfully.";
	}else{
		errorMessage = "Please ensure subject name is unique.";
	}
}


%>

<h4>Insert Subject</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Subject:</td>
				<td><input type="text" id="sub_name"></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
		onclick="insertSubject()">Insert</button>
	<input type="reset" value="Reset" class="btn btn-danger"/>
</form>
