<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.project.database.Student"%>
<%@ page import="java.sql.Connection"%>
<%@ include file="checkSessionStudent.jsp"%>

<%
	String errorMessage = "";
	Student student = new Student();
	Connection connection = student.databaseConnection();
	student.selectStudentInfo(connection, stu_id);
	
	String phone = request.getParameter("phone");
	String address = request.getParameter("address");

	if(phone != null && address != null){
		student.updateStudentInfo(connection, stu_id, phone, address);
	}else if(phone == null && address == null){
		
	}else if(phone == null || address == null){
		errorMessage = "Please ensure all column are not empty.";
	}
	if (errorMessage != "") {
	    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
	}
%>
<h4>Update student information of <%=student.getName() %></h4>
<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Phone:</td>
				<td><input type="text" id="phone"
					value="<%=student.getPhone()%>"></td>
			</tr>
			<tr>
				<td>Address:</td>
				<td><input type="text" id="address"
					value="<%=student.getAddress()%>"></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
	onclick="updateStudentInfo()">Update</button>
	<input type="reset" class="btn btn-info" value="Reset"/>
	<button type="button" class="btn btn-danger"
	onclick="diplayStudentInfo()">Discard</button>
</form>

