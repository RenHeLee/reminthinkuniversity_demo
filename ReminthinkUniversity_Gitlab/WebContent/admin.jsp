<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ include file="checkSessionAdmin.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<title>Admin</title>
</head>
<body>
	<script type="text/javascript">


	function displayArea(type) {
		var wholeArea = document.getElementById("wholeArea");
		var variableUpperArea = document.getElementById("variableUpperArea");
		var variableLowerArea = document.getElementById("variableLowerArea");
		var fixedUpperArea = document.getElementById("fixedUpperArea");
		var fixedLowerArea = document.getElementById("fixedLowerArea");
		if (type == 1) {
			wholeArea.style.display = "block";
			variableUpperArea.style.display = "none";
			variableLowerArea.style.display = "none";
			fixedUpperArea.style.display = "none";
			fixedLowerArea.style.display = "none";
		} else if (type == 2) {
			wholeArea.style.display = "none";
			variableUpperArea.style.display = "block";
			variableLowerArea.style.display = "block";
			fixedUpperArea.style.display = "none";
			fixedLowerArea.style.display = "none";
		} else if (type == 3) {
			wholeArea.style.display = "none";
			variableUpperArea.style.display = "none";
			variableLowerArea.style.display = "none";
			fixedUpperArea.style.display = "block";
			fixedLowerArea.style.display = "block";
		}
	}

	function clearArea() {
		var wholeArea = document.getElementById("wholeArea");
		var variableUpperArea = document.getElementById("variableUpperArea");
		var variableLowerArea = document.getElementById("variableLowerArea");
		var fixedUpperArea = document.getElementById("fixedUpperArea");
		var fixedLowerArea = document.getElementById("fixedLowerArea");
		wholeArea.innerHTML = "";
		variableUpperArea.innerHTML = "";
		variableLowerArea.innerHTML = "";
		fixedUpperArea.innerHTML = "";
		fixedLowerArea.innerHTML = "";
	}
	

	function displayStudentInformationList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectAllStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayStudentInformationByStudentId(stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_id=" + stu_id); 
	}
	
	function displayInsertStudentForm() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_name=&in_sem_id=0&gra_sem_id=0&pho_num=&ic_num=&address="); 
	}

	function displayUpdateStudentForm(stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormUpdateStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_id=" + stu_id + "&stu_name=&in_sem_id=0&gra_sem_id=0&pho_num=&ic_num=&address="); 
	}

	function deleteStudentInformationByStudentId(stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayStudentInformationList();
		    }
		  };
		  xhttp.open("POST", "staffDeleteStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_id=" + stu_id); 
	}

	function updateStudentForm(stu_id) {
		var stu_name = document.getElementById("stu_name").value;
		var in_sem_id = document.getElementById("in_sem_id").value;
		var gra_sem_id = document.getElementById("gra_sem_id").value;
		var pho_num = document.getElementById("pho_num").value;
		var ic_num = document.getElementById("ic_num").value;
		var address = document.getElementById("address").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayStudentInformationList();
		    	displayStudentInformationByStudentId(stu_id);
		    }
		  };
		  xhttp.open("POST", "staffFormUpdateStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_id=" + stu_id + "&stu_name=" + stu_name + "&in_sem_id=" + in_sem_id + "&gra_sem_id=" + gra_sem_id + "&pho_num=" + pho_num + "&ic_num=" + ic_num + "&address=" + address); 	
	}

	function insertStudentForm() {
		var stu_name = document.getElementById("stu_name").value;
		var in_sem_id = document.getElementById("in_sem_id").value;
		var gra_sem_id = document.getElementById("gra_sem_id").value;
		var pho_num = document.getElementById("pho_num").value;
		var ic_num = document.getElementById("ic_num").value;
		var address = document.getElementById("address").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayStudentInformationList();
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_name=" + stu_name + "&in_sem_id=" + in_sem_id + "&gra_sem_id=" + gra_sem_id + "&pho_num=" + pho_num + "&ic_num=" + ic_num + "&address=" + address); 
		
	}
	
	function displayStudentInformation() {
		clearArea();
		displayArea(3);
		displayStudentInformationList();
		displayInsertStudentForm();
	}

	function displayStudentEvaluationListBySubject(sub_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayArea(1);
		    	wholeArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectStudentEvaluationBySubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id); 
	}

	function displayCourseEvaluationListBySubject(sub_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayArea(1);
		    	wholeArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id); 
	}

	function displayStudentListBySubject(sub_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayArea(1);
		    	wholeArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectSubjectList.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id); 
	}

	function displayFormInsertStudentEvaluationByCritetia(ev_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("ev_id=" + ev_id + "&sub_stu_id=0&marks=0&input_type=2"); 
	}

	function displayFormInsertStudentEvaluationByStudent(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id + "&ev_id=0&marks=0&input_type=1"); 
	}

	function insertStudentEvaluationByCriteria(ev_id) {
		var sub_stu_id = document.getElementById("sub_stu_id").value;
		var marks = document.getElementById("marks").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayStudentEvaluationListByCriteria(ev_id);
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("ev_id=" + ev_id + "&sub_stu_id=" + sub_stu_id + "&marks=" + marks + "&input_type=2"); 
	}

	function insertStudentEvaluationByStudent(sub_stu_id) {
		var ev_id = document.getElementById("ev_id").value;
		var marks = document.getElementById("marks").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayStudentEvaluationListByStudent(sub_stu_id);
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id + "&ev_id=" + ev_id + "&marks=" + marks + "&input_type=1"); 
	}
	
	function displayStudentEvaluationListByStudent(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectStudentEvaluationByStudent.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id); 
	}

	function displayStudentEvaluationByStudent(sub_stu_id) {
		displayArea(3);
		fixedUpperArea.style.height = "75%";
		fixedLowerArea.style.height = "25%";
		displayStudentEvaluationListByStudent(sub_stu_id);
		displayFormInsertStudentEvaluationByStudent(sub_stu_id);
	}

	function displayStudentEvaluationListByCriteria(ev_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectStudentEvaluationByCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("ev_id=" + ev_id); 
	}
	

	function displayStudentEvaluationByCriteria(ev_id) {
		displayArea(3);
		fixedUpperArea.style.height = "75%";
		fixedLowerArea.style.height = "25%";
		displayStudentEvaluationListByCriteria(ev_id);
		displayFormInsertStudentEvaluationByCritetia(ev_id);
	}

	function deleteStudentEvaluation(stu_ev_id, id, input_type) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	if(input_type == 1){
		    		displayStudentEvaluationListByStudent(id);
			    }else if(input_type == 2){
			    	displayStudentEvaluationListByCriteria(id);
			    }
		    }
		  };
		  xhttp.open("POST", "staffDeleteStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_ev_id=" + stu_ev_id + "&id=" + id + "&input_type=" + input_type); 
	}

	function displayFormUpdateStudentEvaluation(stu_ev_id, id, input_type) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormUpdateStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_ev_id=" + stu_ev_id + "&id=" + id + "&input_type=" + input_type + "&marks=0"); 
	}

	function updateStudentEvaluation(stu_ev_id, id, input_type) {
		var marks = document.getElementById("marks").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	if(input_type == 1){
		    		displayStudentEvaluationListByStudent(id);
			    }else if(input_type == 2){
			    	displayStudentEvaluationListByCriteria(id);
			    }
		    }
		  };
		  xhttp.open("POST", "staffFormUpdateStudentEvaluation.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_ev_id=" + stu_ev_id + "&id=" + id + "&input_type=" + input_type + "&marks=" + marks); 
	}

	function displaySubjectStudentList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffSelectStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}
	
	function displayInsertSubjectStudentForm() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_id=0&sub_id=0"); 
	}

	function displayUpdateSubjectStudentForm(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "staffFormUpdateStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id + "&sub_id=0"); 
	}

	function deleteSubjectStudent(sub_stu_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySubjectStudentList();
		    }
		  };
		  xhttp.open("POST", "staffDeleteStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id); 
	}

	function updateSubjectStudent(sub_stu_id) {
		var sub_id = document.getElementById("sub_id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displaySubjectStudentList();
				displayInsertSubjectStudentForm();
		    }
		  };
		  xhttp.open("POST", "staffFormUpdateStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_stu_id=" + sub_stu_id + "&sub_id=" + sub_id); 	
	}

	function insertSubjectStudent() {
		var stu_id = document.getElementById("stu_id").value;
		var sub_id = document.getElementById("sub_id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySubjectStudentList();
		    }
		  };
		  xhttp.open("POST", "staffFormInsertStudentSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("stu_id=" + stu_id + "&sub_id=" + sub_id); 
		
	}

	function displayStudentSubject() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "75%";
		fixedLowerArea.style.height = "25%";
		displaySubjectStudentList();
		displayInsertSubjectStudentForm();
	}


	function displayStaffInformationList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllStaffInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertStaffInformation() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertStaffInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sta_name=&sta_po=0&fir_ser_date=&las_ser_date=&pho_num=&ic_num=&address="); 
	}
	

	function displayUpdateStaffInformation(sta_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateStaffInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sta_id=" + sta_id + "&sta_name=&sta_po=0&fir_ser_date=&las_ser_date=&pho_num=&ic_num=&address="); 
	}

	function deleteStaffInformation(id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayStaffInformationList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteStaffInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("id=" + id); 
	}

	function updateStaffInformation(sta_id) {
		var sta_name = document.getElementById("sta_name").value;
		var sta_po = document.getElementById("sta_po").value;
		var fir_ser_date = document.getElementById("fir_ser_date").value;
		var las_ser_date = document.getElementById("las_ser_date").value;
		var pho_num = document.getElementById("pho_num").value;
		var ic_num = document.getElementById("ic_num").value;
		var address = document.getElementById("address").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displayStaffInformationList();
		    	displayInsertStaffInformation();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateStaffInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sta_name=" + sta_name + "&sta_po=" + sta_po + "&fir_ser_date=" + fir_ser_date + "&las_ser_date=" + las_ser_date + "&pho_num=" + pho_num + "&ic_num=" + ic_num + "&address=" + address + "&sta_id=" + sta_id); 	
	}

	function insertStaffInformation() {
		var sta_name = document.getElementById("sta_name").value;
		var sta_po = document.getElementById("sta_po").value;
		var fir_ser_date = document.getElementById("fir_ser_date").value;
		var las_ser_date = document.getElementById("las_ser_date").value;
		var pho_num = document.getElementById("pho_num").value;
		var ic_num = document.getElementById("ic_num").value;
		var address = document.getElementById("address").value;

		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayStaffInformationList();
		    }
		  };

		  xhttp.open("POST", "adminFormInsertStaffInfo.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sta_name=" + sta_name + "&sta_po=" + sta_po + "&fir_ser_date=" + fir_ser_date + "&las_ser_date=" + las_ser_date + "&pho_num=" + pho_num + "&ic_num=" + ic_num + "&address=" + address); 	
	}
	
	function displayStaffInformation() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "50%";
		fixedLowerArea.style.height = "50%";
		displayStaffInformationList();
		displayInsertStaffInformation();
	}
 
 	function displaySubjectList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertSubject() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_name="); 
	}
	

	function displayUpdateSubject(sub_co) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_co=" + sub_co + "&sub_name="); 
	}

	function deleteSubject(sub_co) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySubjectList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_co=" + sub_co); 
	}

	function updateSubject(sub_co) {
		var sub_name = document.getElementById("sub_name").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displaySubjectList();
		    	displayInsertSubject();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_co=" + sub_co + "&sub_name=" + sub_name); 	
	}

	function insertSubject() {
		var sub_name = document.getElementById("sub_name").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySubjectList();
		    }
		  };

		  xhttp.open("POST", "adminFormInsertSubject.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_name=" + sub_name); 	
	}
	
	function displaySubject() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "75%";
		fixedLowerArea.style.height = "25%";
		displaySubjectList();
		displayInsertSubject();
	} 

   	function displayEvaluateMethodList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllEvaluateMethod.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertEvaluateMethod() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertEvaluateMethod.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("me_name="); 
	}
	

	function displayUpdateEvaluateMethod(me_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateEvaluateMethod.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("me_id=" + me_id + "&me_name="); 
	}

	function deleteEvaluateMethod(me_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayEvaluateMethodList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteEvaluateMethod.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("me_id=" + me_id); 
	}

	function updateEvaluateMethod(me_id) {
		var me_name = document.getElementById("me_name").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 & this.status == 200) {
		    	displayEvaluateMethodList();
		    	displayInsertEvaluateMethod();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateEvaluateMethod.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("me_id=" + me_id + "&me_name=" + me_name); 	
	}

	function insertEvaluateMethod() {
		var me_name = document.getElementById("me_name").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayEvaluateMethodList();
		    }
		  };

		  xhttp.open("POST", "adminFormInsertEvaluateMethod.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("me_name=" + me_name); 	
	}
	
	function displayEvaluateMethod() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "75%";
		fixedLowerArea.style.height = "25%";
		displayEvaluateMethodList();
		displayInsertEvaluateMethod();
	} 

 	   	function displaySemDateList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllSemDate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertSemDate() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertSemDate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_co=&in_date=&gra_date="); 
	}
	

	function displayUpdateSemDate(sem_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateSemDate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_id=" + sem_id + "&sem_co=&in_date=&gra_date="); 
	}

	function deleteSemDate(sem_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySemDateList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteSemDate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_id=" + sem_id); 
	}

	function updateSemDate(sem_id) {
		var sem_co = document.getElementById("sem_co").value;
		var in_date = document.getElementById("in_date").value;
		var gra_date = document.getElementById("gra_date").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 & this.status == 200) {
		    	displaySemDateList();
		    	displayInsertSemDate();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateSemDate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_id=" + sem_id + "&sem_co=" + sem_co + "&in_date=" + in_date + "&gra_date=" + gra_date); 	
	}

	function insertSemDate() {
		var sem_co = document.getElementById("sem_co").value;
		var in_date = document.getElementById("in_date").value;
		var gra_date = document.getElementById("gra_date").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySemDateList();
		    }
		  };

		  xhttp.open("POST", "adminFormInsertSemDate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sem_co=" + sem_co + "&in_date=" + in_date + "&gra_date=" + gra_date); 	
	}
	
	function displaySemDate() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "50%";
		fixedLowerArea.style.height = "50%";
		displaySemDateList();
		displayInsertSemDate();
	} 


	 function displayWorkloadList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllWorkload.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertWorkload() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertWorkload.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=0&id=0"); 
	}
	

	function displayUpdateWorkload(wl_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateWorkload.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("wl_id=" + wl_id +"&sub_id=0&id=0"); 
	}

	function deleteWorkload(wl_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayWorkloadList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteWorkload.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("wl_id=" + wl_id); 
	}

	function updateWorkload(wl_id) {
		var sub_id = document.getElementById("sub_id").value;
		var id = document.getElementById("id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 & this.status == 200) {
		    	displayWorkloadList();
		    	displayInsertWorkload();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateWorkload.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("wl_id=" + wl_id + "&sub_id=" + sub_id + "&id=" + id); 	
	}

	function insertWorkload() {
		var sub_id = document.getElementById("sub_id").value;
		var id = document.getElementById("id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayWorkloadList();
		    }
		  };
		  xhttp.open("POST", "adminFormInsertWorkload.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id + "&id=" + id); 	
	}
	
	function displayWorkload() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "50%";
		fixedLowerArea.style.height = "50%";
		displayWorkloadList();
		displayInsertWorkload();
	} 

	function displayCourseEvaluateList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertCourseEvaluate() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=0&me_id=0&ev_weightage=0"); 
	}
	

	function displayUpdateCourseEvaluate(ev_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("ev_id=" + ev_id + "&sub_id=0&me_id=0&ev_weightage=0"); 
	}

	function deleteCourseEvaluate(ev_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayCourseEvaluateList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("ev_id=" + ev_id); 
	}

	function updateCourseEvaluate(ev_id) {
		var sub_id = document.getElementById("sub_id").value;
		var me_id = document.getElementById("me_id").value;
		var ev_weightage = document.getElementById("ev_weightage").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 & this.status == 200) {
		    	displayCourseEvaluateList();
		    	displayInsertCourseEvaluate();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("ev_id=" + ev_id + "&sub_id=" + sub_id + "&me_id=" + me_id + "&ev_weightage=" + ev_weightage); 	
	}

	function insertCourseEvaluate() {
		var sub_id = document.getElementById("sub_id").value;
		var me_id = document.getElementById("me_id").value;
		var ev_weightage = document.getElementById("ev_weightage").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displayCourseEvaluateList();
		    }
		  };

		  xhttp.open("POST", "adminFormInsertCourseEvaluate.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id + "&me_id=" + me_id + "&ev_weightage=" + ev_weightage); 	
	}
	
	function displayCourseEvaluate() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "50%";
		fixedLowerArea.style.height = "50%";
		displayCourseEvaluateList();
		displayInsertCourseEvaluate();
	} 


	function displaySubjectBySectionList() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedUpperArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminSelectAllSubjectBySection.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send(); 
	}

	function displayInsertSubjectBySection() {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormInsertSubjectBySection.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_co=0&sec=0&sem_id=0"); 
	}
	

	function displayUpdateSubjectBySection(sub_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateSubjectBySection.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id + "&sub_co=0&sec=0&sem_id=0"); 
	}

	function deleteSubjectBySection(sub_id) {
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySubjectBySectionList();
		    }
		  };
		  xhttp.open("POST", "adminDeleteSubjectBySection.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id); 
	}

	function updateSubjectBySection(sub_id) {
		var sub_co = document.getElementById("sub_co").value;
		var sec = document.getElementById("sec").value;
		var sem_id = document.getElementById("sem_id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	displaySubjectBySectionList();
		    	displayInsertSubjectBySection();
		    }
		  };
		  xhttp.open("POST", "adminFormUpdateSubjectBySection.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_id=" + sub_id + "&sub_co=" + sub_co + "&sec=" + sec + "&sem_id=" + sem_id); 	
	}

	function insertSubjectBySection() {
		var sub_co = document.getElementById("sub_co").value;
		var sec = document.getElementById("sec").value;
		var sem_id = document.getElementById("sem_id").value;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	fixedLowerArea.innerHTML = this.responseText;
		    	displaySubjectBySectionList();
		    }
		  };

		  xhttp.open("POST", "adminFormInsertSubjectBySection.jsp", true);
		  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  xhttp.send("sub_co=" + sub_co + "&sec=" + sec + "&sem_id=" + sem_id); 	
	}
	
	function displaySubjectBySection() {
		clearArea();
		displayArea(3);
		fixedUpperArea.style.height = "60%";
		fixedLowerArea.style.height = "40%";
		displaySubjectBySectionList();
		displayInsertSubjectBySection();
	} 

	
</script>


	<div class="container-fluid.mt-0"
		style="height: 864px; background-color: Goldenrod;">
		<div id="header" style="height: 10%">
			<nav class="navbar navbar-expand-sm bg-light navbar-light">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="">Admin
						of Reminthink University</a></li>
				
				<li class="nav-item dropdown">
			      <a class="nav-link dropdown-toggle" data-toggle="dropdown">
			        Student Management
			      </a>
			      <div class="dropdown-menu">
			        <a class="dropdown-item" href="javascript:displayStudentInformation();">Student Information</a>
			        <a class="dropdown-item" href="javascript:displayStudentSubject()">Students' Subject</a>
			      </div>
			    </li>
			    
			    <li class="nav-item dropdown">
			      <a class="nav-link dropdown-toggle" data-toggle="dropdown">
			        Staff Management
			      </a>
			      <div class="dropdown-menu">
			        <a class="dropdown-item" href="javascript:displayEvaluateMethod()">Evaluation Method</a>
			        <a class="dropdown-item" href="javascript:displaySemDate()">Semester Date</a>
			        <a class="dropdown-item" href="javascript:displayStaffInformation();">Staff Information</a>
			        <a class="dropdown-item" href="javascript:displaySubject()">Subject</a>
			        <a class="dropdown-item" href="javascript:displayCourseEvaluate()">Subject Evaluation Criteria</a>
			        <a class="dropdown-item" href="javascript:displaySubjectBySection()">Subject by Section</a>
			        <a class="dropdown-item" href="javascript:displayWorkload()">Workload</a>
			      </div>
			    </li>
			
			</ul>
			</nav>
		</div>
		<div id="content" style="height: 85%;">
			<div
				style="background-image: url('img/img_0005.jpg'); height: 100%; background-size: cover; background-repeat: no-repeat; background-position: center;">
				<div id="wholeArea" style="background-color: rgba(250, 230, 230, 0.70); height: 100%; width: 100%; overflow-y: scroll; padding: 10px 10px 10px 10px;">
				<h1 align="center">Welcome to Reminthink University Management System (Admin Version)</h1>
				</div>
				<div id="variableUpperArea" style="background-color: rgba(230, 250, 230, 0.70); padding: 10px 10px 10px 10px;"></div>
				<div id="variableLowerArea" style="background-color: rgba(230, 230, 250, 0.70); padding: 10px 10px 10px 10px;"></div>
				<div id="fixedUpperArea" style="background-color: rgba(230, 250, 230, 0.70); height: 50%; width: 100%; overflow-y: scroll; padding: 10px 10px 10px 10px;"></div>
				<div id="fixedLowerArea" style="background-color: rgba(230, 230, 250, 0.70); height: 50%; width: 100%; overflow-y: scroll; padding: 10px 10px 10px 10px;"></div>
			</div>
		</div>
		<div id="footer" style="height: 5%;"><jsp:include page="footer.jsp"></jsp:include></div>
	</div>
	<script type="text/javascript">displayArea(1);</script>
	
	
</body>
</html>