<%@page import="com.project.database.Lecturer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

staff.selectAllEvaluateMethod(connection);

%>

<h4>Evaluate method information</h4>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>No.</td>
			<td>Method name</td>
			<td>Modified by</td>
			<td>Modified date</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
		
		<%
		for (int i = 0; i < staff.getMeName().size(); i++) {
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getMeName().get(i) + "</td>");			
			out.println("<td>" + staff.getModBy().get(i) + "</td>");
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-warning' onclick='displayUpdateEvaluateMethod(" + staff.getMeId().get(i) + ")'>Update</button></td>");
			out.println("<td><button type='button' class='btn btn-danger' onclick='deleteEvaluateMethod(" + staff.getMeId().get(i) + ")'>Delete</button></td></tr>");
		}
		%>
	</tbody>
</table>