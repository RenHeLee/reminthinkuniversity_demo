<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

String sta_name = request.getParameter("sta_name");
int sta_po = Integer.parseInt(request.getParameter("sta_po"));
Date fir_ser_date=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("fir_ser_date"));
Date las_ser_date=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("las_ser_date"));
String pho_num = request.getParameter("pho_num");
String ic_num = request.getParameter("ic_num");
String address = request.getParameter("address");
int id = Integer.parseInt(request.getParameter("id"));

out.print(staff.updateStaffInfo(connection, sta_name, sta_po, fir_ser_date, las_ser_date, pho_num, ic_num, address, sta_id, id));
%>
