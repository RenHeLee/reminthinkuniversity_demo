<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionLecturer.jsp"%>

<%
	Lecturer staff = new Lecturer();
	Connection connection = staff.databaseConnection();
	
	int semId = 0;
	if (request.getParameter("semId") != null) {
		semId = Integer.parseInt(request.getParameter("semId"));
		staff.selectWorkload(connection, sta_id, semId);
	} else {
		staff.selectWorkload(connection, sta_id);
	}
%>
<h4>Workload 
<% 
if(semId != 0){
	out.println(" of " + staff.selectSemCode(connection, semId));
}
%>
</h4>
<table class="table tabfle-hover">
	<tbody>
		<tr>
			<td rowspan="2">No.</td>
			<td rowspan="2">Subject</td>
			<%
			if(semId == 0){
				out.println("<td rowspan='2'>Semester</td>");
			}
			%>
			<td rowspan="2">Section</td>
			<td rowspan="2">Modified by</td>
			<td rowspan="2">Modified date</td>
			<td colspan="2">Evaluation</td>
			<td rowspan="2">Student</td>
		</tr>
		<tr>
			<td>Criteria</td>
			<td>Overall</td>
		</tr>
		<%
		for(int i = 0; i < staff.getSec().size(); i++){
			out.println("<tr><td>" + (i + 1) + "</td>");
			out.println("<td>" + staff.getSubName().get(i) + "</td>");
			if(semId == 0){
				out.println("<td><a href='javascript:displaySelectionWorkloadBySemFromList(" + staff.getSemId().get(i) + ")'>" + staff.getSemCo().get(i) + "</td>");
			}
			out.println("<td>" + staff.getSec().get(i) + "</td>");
			out.println("<td>" + staff.getModBy().get(i) + "</td>");
			out.println("<td>" + staff.getModDate().get(i) + "</td>");
			out.println("<td><button type='button' class='btn btn-info' onclick='displayCourseEvaluationListBySubject(" + staff.getSubId().get(i) + ")'>Criteria</button></td>");
			out.println("<td><button type='button' class='btn btn-primary' onclick='displayStudentEvaluationListBySubject(" + staff.getSubId().get(i) + ")'>Overall</button></td>");
			out.println("<td><button type='button' class='btn btn-primary' onclick='displayStudentListBySubject(" + staff.getSubId().get(i) + ")'>Student</button></td></tr>");
		}
		%>
	</tbody>
</table>
