<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionLecturer.jsp"%>

<%
String errorMessage = "";
Lecturer staff = new Lecturer();
Connection connection = staff.databaseConnection();

staff.selectStaffInfo(connection, sta_id);

String phone = request.getParameter("phone");
String address = request.getParameter("address");

if(phone != null && address != null){
	staff.updateStaffInfo(connection, sta_id, phone, address);
}else if(phone == null && address == null){
	
}else if(phone == null || address == null){
	errorMessage = "Please ensure all column are not empty.";
}
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
}
%>

<form>
	<table class="table table-hover">
		<tbody>
			<tr>
				<td>Phone:</td>
				<td><input type="text" id="phone"
					value="<%=staff.getPhone()%>"></td>
			</tr>
			<tr>
				<td>Address:</td>
				<td><input type="text" id="address"
					value="<%=staff.getAddress()%>"></td>
			</tr>
		</tbody>
	</table>
	<button type="button" class="btn btn-success"
	onclick="updateLecturerInfo()">Update</button>
	<input type="reset" class="btn btn-info" value="Reset"/>
	<button type="button" class="btn btn-danger"
	onclick="diplayStaffInfo()">Discard</button>
</form>