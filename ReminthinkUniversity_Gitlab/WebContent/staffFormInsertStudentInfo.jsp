<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();
String successMessage = "";
String errorMessage = "";
String stu_name = request.getParameter("stu_name");
int in_sem_id = Integer.parseInt(request.getParameter("in_sem_id"));
int gra_sem_id = Integer.parseInt(request.getParameter("gra_sem_id"));
String pho_num = request.getParameter("pho_num");
String ic_num = request.getParameter("ic_num");
String address = request.getParameter("address");

if(stu_name == "" && in_sem_id == 0 && gra_sem_id == 0 && pho_num == "" && ic_num == "" && address == ""){
}else if (stu_name != "" && in_sem_id != 0 && gra_sem_id != 0 && pho_num != "" && ic_num != "" && address != ""){
	if(staff.checkStudentInfo(connection, ic_num)){
		staff.insertStudentInfo(connection, stu_name, in_sem_id, gra_sem_id, pho_num, ic_num, address, sta_id);
		successMessage = "This student's record is inserted successfully.";
	}else{
		errorMessage = "Please ensure IC number is unique.";
	}
}else if(stu_name == "" || in_sem_id == 0 || gra_sem_id == 0 || pho_num == "" || ic_num == "" || address == "" ){
	errorMessage = "Please ensure all column are not empty.";
}

%>

<h4>Insert Student Information</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>Name: </td>
			<td><input type="text" id="stu_name"></td>
		</tr>
		<tr>
			<td>Intake Semester: </td>
			<td><select id='in_sem_id'>
			<option value="">Select semester for intake</option>
			<%
				staff.selectAllSemDate(connection);
				for (int i = 0; i < staff.getSemId().size(); i++){
					out.println("<option value="+ staff.getSemId().get(i) +">" + staff.getSemCo().get(i) + "</option>");
				}
			%>
	</select></td>
		</tr>
		<tr>
			<td>Graduate Semester: </td>
			<td><select id='gra_sem_id'>
			<option value="">Select semester for graduate</option>
			<%
				for (int i = 0; i < staff.getSemId().size(); i++){
					out.println("<option value="+ staff.getSemId().get(i) +">" + staff.getSemCo().get(i) + "</option>");
				}
			%>
	</select></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><input type="text" id="pho_num"></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><input type="text" id="ic_num"></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><input type="text" id="address"></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-success" onclick="insertStudentForm()">Insert</button>
<input type="reset" class="btn btn-danger" value="Reset">
</form>