<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

Date in_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("in_date"));
Date gra_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("gra_date"));
String sem_co = request.getParameter("sem_co");


if(staff.checkSemDate(connection, sem_co)){
	out.print(staff.insertSemDate(connection, in_date, gra_date, sem_co, sta_id));
}else{
	out.print("Repeated");
}
%>
