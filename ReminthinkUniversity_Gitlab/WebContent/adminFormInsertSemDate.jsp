<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>


<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

String successMessage = "";
String errorMessage = "";
String sem_co = request.getParameter("sem_co");
String firstDate = request.getParameter("in_date");
String lastDate = request.getParameter("gra_date");
Date in_date = null;
Date gra_date = null;

if(firstDate != ""){
	in_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("in_date"));
}
if(lastDate != ""){
	gra_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("gra_date"));
}



if(firstDate == "" && lastDate == "" && sem_co == ""){
}else if (firstDate != "" && lastDate != "" && sem_co != ""){
	if(staff.checkSemDate(connection, sem_co)){
		staff.insertSemDate(connection, in_date, gra_date, sem_co, sta_id);
		successMessage = "This semester record is inserted successfully.";
	}else{
		errorMessage = "Please ensure semester code is unique.";
	}
}else{
	errorMessage = "Please ensure all column are not empty.";
}
%>


<h4>Insert semester information</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>Semester: </td>
			<td><input type="text" id="sem_co" value=""></td>
		</tr>
		<tr>
			<td>Starting from: </td>
			<td><input type="date" id="in_date" value=""></td>
		</tr>
		<tr>
			<td>Until to: </td>
			<td><input type="date" id="gra_date" value=""></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-success" onclick="insertSemDate()">Insert</button>
<input type="reset" value="Reset" class="btn btn-warning" >
</form>
