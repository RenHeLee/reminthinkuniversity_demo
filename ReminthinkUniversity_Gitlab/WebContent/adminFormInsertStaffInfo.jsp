<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdmin.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();

String successMessage = "";
String errorMessage = "";
String sta_name = request.getParameter("sta_name");
int sta_po = Integer.parseInt(request.getParameter("sta_po"));
String firstDate = request.getParameter("fir_ser_date");
String lastDate = request.getParameter("las_ser_date");

Date fir_ser_date = null;
Date las_ser_date = null;

if(firstDate != ""){
	fir_ser_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("fir_ser_date"));
}
if(lastDate != ""){
	las_ser_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("las_ser_date"));
}

String pho_num = request.getParameter("pho_num");
String ic_num = request.getParameter("ic_num");
String address = request.getParameter("address");


if(sta_name == "" && sta_po == 0 && firstDate == "" && firstDate == "" && pho_num  == "" && ic_num == "" && address == ""){
}else if (sta_name != "" && sta_po != 0 && firstDate != "" && firstDate != "" && pho_num  != "" && ic_num != "" && address != ""){
	if(staff.checkStaffInfo(connection, ic_num)){
		staff.insertStaffInfo(connection, sta_name, sta_po, fir_ser_date, las_ser_date, pho_num, ic_num, address, sta_id);
		successMessage = "This student's record is inserted successfully.";
	}else{
		errorMessage = "Please ensure IC number is unique.";
	}
}else{
	errorMessage = "Please ensure all column are not empty.";
}

%>
<h4>Insert staff information</h4>
<% 
if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + sta_name + sta_po + firstDate + firstDate + pho_num + ic_num + address + "</div>");
} else if (successMessage != "") {
    out.print("<div class='alert alert-success'><strong>Success: </strong>" + successMessage + "</div>");
} 
%>
<form>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>Name: </td>
			<td><input type="text" id="sta_name"></td>
		</tr>
		<tr>
			<td>Position: </td>
			<td>
				<select id="sta_po" form="sta_po">
				  <%
				  	staff.selectAllPosition(connection);
				  	for(int i = 0; i < staff.getPoTitle().size(); i++){
				  		out.println("<option value=" + staff.getPoId().get(i) + ">" + staff.getPoTitle().get(i) + "</option>");
				  	}
				  %>
				</select>

			</td>
		</tr>
		<tr>
			<td>First service date: </td>
			<td><input type="date" id="fir_ser_date"></td>
		</tr>
		<tr>
			<td>Last service date: </td>
			<td><input type="date" id="las_ser_date"></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><input type="text" id="pho_num"></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><input type="text" id="ic_num"></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><input type="text" id="address"></td>
		</tr>
	</tbody>
</table>
<button type="button" class="btn btn-info" onclick="insertStaffInformation()">Insert</button>
<input type="reset" class="btn btn-danger" value="Reset">
</form>