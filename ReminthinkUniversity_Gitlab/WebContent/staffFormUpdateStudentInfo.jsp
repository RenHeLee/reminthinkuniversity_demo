<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="checkSessionAdminLecturer.jsp"%>

<%
Admin staff = new Admin();
Connection connection = staff.databaseConnection();
String errorMessage = "";
int stu_id = Integer.parseInt(request.getParameter("stu_id"));
String stu_name = request.getParameter("stu_name");
int in_sem_id = Integer.parseInt(request.getParameter("in_sem_id"));
int gra_sem_id = Integer.parseInt(request.getParameter("gra_sem_id"));
String pho_num = request.getParameter("pho_num");
String ic_num = request.getParameter("ic_num");
String address = request.getParameter("address");

staff.selectStudentInfo(connection, stu_id);

if(stu_name == "" && in_sem_id == 0 && gra_sem_id == 0 && pho_num == "" && ic_num == "" && address == ""){
	
}else if (stu_name != "" && in_sem_id != 0 && gra_sem_id != 0 && pho_num != "" && ic_num != "" && address != ""){
	staff.updateStudentInfo( connection, stu_name, in_sem_id, gra_sem_id, pho_num, ic_num, address, sta_id, stu_id);
}else if(stu_name != "" || in_sem_id != 0 || gra_sem_id != 0 || pho_num != "" || ic_num != "" || address != "" ){
	errorMessage = "Please ensure all column are not empty.";
}
 

%>

<h4>Update Student Information of <%=staff.getName() %></h4>
<% if (errorMessage != "") {
    out.print("<div class='alert alert-danger'><strong>Error: </strong>" + errorMessage + "</div>");
} %>
<form>
<table class="table table-hover">
	<tbody>
		<tr>
			<td>ID: </td>
			<td><input type="text" id="stu_id" value="<% out.println(staff.getPo_co() + stu_id); %>" disabled="disabled"></td>
		</tr>
		<tr>
			<td>Name: </td>
			<td><input type="text" id="stu_name" value="<%=staff.getName() %>"></td>
		</tr>
		<tr>
			<td>Intake Semester: </td>
			<td><select id='in_sem_id'>
			<%
			out.println("<option value="+ staff.getIntake_sem_id() +">" + staff.getIntake_sem_co() + "</option>");
				staff.selectAllSemDate(connection);
				for (int i = 0; i < staff.getSemId().size(); i++){
					out.println("<option value="+ staff.getSemId().get(i) +">" + staff.getSemCo().get(i) + "</option>");
				}
			%>
	</select></td>
		</tr>
		<tr>
			<td>Graduate Semester: </td>
			<td><select id='gra_sem_id'>
			<%
			out.println("<option value="+ staff.getGraduate_sem_id() +">" + staff.getGraduate_sem_co() + "</option>");
				for (int i = 0; i < staff.getSemId().size(); i++){
					out.println("<option value="+ staff.getSemId().get(i) +">" + staff.getSemCo().get(i) + "</option>");
				}
			%>
	</select></td>
		</tr>
		<tr>
			<td>Phone: </td>
			<td><input type="text" id="pho_num" value="<%=staff.getPhone() %>"></td>
		</tr>
		<tr>
			<td>IC: </td>
			<td><input type="text" id="ic_num" value="<%=staff.getIc() %>" disabled="disabled"></td>
		</tr>
		<tr>
			<td>Address: </td>
			<td><input type="text" id="address" value="<%=staff.getAddress() %>"></td>
		</tr>
	</tbody>
</table>

<button type="button" class="btn btn-success" onclick="updateStudentForm(<%= stu_id%>)">Update</button>
<input type="reset" class="btn btn-warning" value="Reset">
<button type="button" class="btn btn-danger" onclick="displayInsertStudentForm()">Discard</button></td>
</form>